﻿using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Dps5315;
using Dps5315LiIoCharger.Events;
using Dps5315LiIoCharger.Services;
using Dps5315LiIoCharger.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shouldly;
using System;
using System.Collections.ObjectModel;

namespace Dps5315LiIoChargerTests.ViewModels
{
    [TestClass]
    public class MainWindowViewModelTests
    {
        private const int ChannelNumber = 0;
        private const string Setup1 = "Setup 1";
        private const string Setup2 = "Setup 2";
        Mock<IDpsCommunication> _dpsCommunicationMock;
        Mock<IChannelViewModel> _masterChannelViewModelMock;
        Mock<IChannelViewModel> _slaveChannelViewModelMock;
        Mock<IConfigService> _configServiceMock;
        Mock<IMessagingService> _messagingServiceMock;
        private Configuration _configuration;

        [TestInitialize]
        public void TestInitialize()
        {
            _dpsCommunicationMock = new Mock<IDpsCommunication>();
            _masterChannelViewModelMock = new Mock<IChannelViewModel>();
            _slaveChannelViewModelMock = new Mock<IChannelViewModel>();
            _configServiceMock = new Mock<IConfigService>();
            _messagingServiceMock = new Mock<IMessagingService>();

            _configuration = new Configuration();
            _configServiceMock.SetupGet(x => x.Configuration).Returns(_configuration);
        }

        [TestMethod]
        public void Create()
        {
            Should.Throw<ArgumentNullException>(() => new MainWindowViewModel(null, null, null, null, null));
            Should.Throw<ArgumentNullException>(() => new MainWindowViewModel(_dpsCommunicationMock.Object, null, null, null, null));
            Should.Throw<ArgumentNullException>(() => new MainWindowViewModel(_dpsCommunicationMock.Object, _masterChannelViewModelMock.Object, null, null, null));
            Should.Throw<ArgumentNullException>(() => new MainWindowViewModel(_dpsCommunicationMock.Object, _masterChannelViewModelMock.Object, _slaveChannelViewModelMock.Object, null, null));
            Should.Throw<ArgumentNullException>(() => new MainWindowViewModel(_dpsCommunicationMock.Object, _masterChannelViewModelMock.Object, _slaveChannelViewModelMock.Object, _configServiceMock.Object, null));
            var sut = NewSut();
            sut.ShouldNotBeNull();
            _masterChannelViewModelMock.VerifySet(x => x.IsChannelConnected = false);
            _masterChannelViewModelMock.Verify(x => x.Init(0));
            _slaveChannelViewModelMock.Verify(x => x.Init(1));
        }

        [TestMethod]
        public void ChannelLimitsChanging_Stopped()
        {
            var sut = NewSut();
            Should.Throw<ArgumentException>(() => _masterChannelViewModelMock.Raise(x => x.ChannelLimitsChanging += null, null, new ChannelLimitsChangingArgs(0, 0)));

            _masterChannelViewModelMock.SetupGet(x => x.ChannelState).Returns(ChannelState.Stopped);
            _masterChannelViewModelMock.Raise(x => x.ChannelLimitsChanging += null, _masterChannelViewModelMock.Object, new ChannelLimitsChangingArgs(0, 0));
            _masterChannelViewModelMock.VerifyGet(x => x.ChannelState, Times.Once);
            _masterChannelViewModelMock.VerifyGet(x => x.ChargingVoltage, Times.Never());
            _masterChannelViewModelMock.VerifyGet(x => x.ChargingCurrent, Times.Never());
            _slaveChannelViewModelMock.VerifyGet(x => x.ChargingVoltage, Times.Never());
            _slaveChannelViewModelMock.VerifyGet(x => x.ChargingCurrent, Times.Never());
            _dpsCommunicationMock.Verify(x => x.WriteRead<SetLimitsResponse>(It.IsAny<SetLimitsRequest>()), Times.Never);
        }

        [TestMethod]
        public void ChannelLimitsChanging_Started()
        {
            const double mVoltage = 10;
            const double mCurrent = 1.5;
            const double sVoltage = 4.2;
            const double sCurrent = 0.5;
            SetLimitsRequest receivedRequest = null;
            var sut = NewSut();
            _masterChannelViewModelMock.SetupGet(x => x.ChannelState).Returns(ChannelState.Started);
            _slaveChannelViewModelMock.SetupGet(x => x.ChargingVoltage).Returns(sVoltage);
            _slaveChannelViewModelMock.SetupGet(x => x.ChargingCurrent).Returns(sCurrent);
            _dpsCommunicationMock.Setup(x => x.WriteRead<SetLimitsResponse>(It.IsAny<SetLimitsRequest>())).Callback((IDpsRequest r) => {receivedRequest = r as SetLimitsRequest;});
            var args = new ChannelLimitsChangingArgs(mVoltage, mCurrent);
            _masterChannelViewModelMock.Raise(x => x.ChannelLimitsChanging += null, _masterChannelViewModelMock.Object, args);
            receivedRequest.ShouldNotBeNull();
            receivedRequest.LimitsUI.Master.Voltage.ShouldBe(mVoltage);
            receivedRequest.LimitsUI.Master.Current.ShouldBe(mCurrent);
            receivedRequest.LimitsUI.Slave.Voltage.ShouldBe(sVoltage);
            receivedRequest.LimitsUI.Slave.Current.ShouldBe(sCurrent);

            args.CanChange.ShouldBeTrue();
        }

        [TestMethod]
        public void ChannelLimitsChanging_Started_Fail()
        {
            var sut = NewSut();
            _masterChannelViewModelMock.SetupGet(x => x.ChannelState).Returns(ChannelState.Started);
            _dpsCommunicationMock.Setup(x => x.WriteRead<SetLimitsResponse>(It.IsAny<SetLimitsRequest>())).Throws<InvalidOperationException>();
            var args = new ChannelLimitsChangingArgs(0, 0);
            _masterChannelViewModelMock.Raise(x => x.ChannelLimitsChanging += null, _masterChannelViewModelMock.Object, args);
            args.CanChange.ShouldBeFalse();
        }

        private MainWindowViewModel NewSut()
        {
            var sut = new MainWindowViewModel(_dpsCommunicationMock.Object, _masterChannelViewModelMock.Object, _slaveChannelViewModelMock.Object, _configServiceMock.Object, _messagingServiceMock.Object);
            return sut;
        }
    }
}
