﻿using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Services;
using Dps5315LiIoCharger.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OxyPlot;
using Shouldly;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Dps5315LiIoChargerTests.ViewModels
{
    [TestClass]
    public class ChannelViewModelTests
    {
        private const int ChannelNumber = 0;
        private const string Setup1 = "Setup 1";
        private const string Setup2 = "Setup 2";
        private const LegendPosition TestLegendPosition = LegendPosition.TopLeft;
        private ObservableCollection<string> _availableSetups;
        private Configuration _configuration;
        Mock<IChannelInputViewModel> _channelInputMock;
        Mock<IChannelGraphViewModel> _channelGraphMock;
        Mock<IChannelProgressViewModel> _channelProgressMock;
        Mock<IConfigService> _configServiceMock;
        Mock<IDateTimeService> _dateTimeServiceMock;

        [TestInitialize]
        public void TestInitialize()
        {
            _channelInputMock = new Mock<IChannelInputViewModel>();
            _channelGraphMock = new Mock<IChannelGraphViewModel>();
            _channelProgressMock = new Mock<IChannelProgressViewModel>();
            _configServiceMock = new Mock<IConfigService>();
            _dateTimeServiceMock = new Mock<IDateTimeService>();

            _availableSetups = new ObservableCollection<string> { Setup1, Setup2 };
            _configuration = new Configuration();
            _configuration.SetLastSetup(ChannelNumber, Setup2);

            _configServiceMock.SetupGet(x => x.Configuration).Returns(_configuration);
        }

        [TestMethod]
        public void Create()
        {
            Should.Throw<ArgumentNullException>(() => new ChannelViewModel(null, null, null, null, null));
            Should.Throw<ArgumentNullException>(() => new ChannelViewModel(_channelInputMock.Object, null, null, null, null));
            Should.Throw<ArgumentNullException>(() => new ChannelViewModel(_channelInputMock.Object, _channelGraphMock.Object, null, null, null));
            Should.Throw<ArgumentNullException>(() => new ChannelViewModel(_channelInputMock.Object, _channelGraphMock.Object, _channelProgressMock.Object, null, null));
            Should.Throw<ArgumentNullException>(() => new ChannelViewModel(_channelInputMock.Object, _channelGraphMock.Object, _channelProgressMock.Object, _configServiceMock.Object, null));
            var sut = NewSut();
            sut.ShouldNotBeNull();
            sut.ChannelState.ShouldBe(ChannelState.Stopped);
        }

        [TestMethod]
        public void Init()
        {
            var sut = NewSut(ChannelNumber);
            sut.ChannelInput.SelectedSetupIndex = 1;
            sut.ChannelGraph.SelectedLegendPosition = _configuration.LegendPositions[0];
        }

        [TestMethod]
        public void ChannelState_Stopped()
        {
            var channelStateChangingCalled = false;
            var sut = NewSut(ChannelNumber);
            sut.ChannelStateChanging += (sender, args) => { channelStateChangingCalled = true; };
            sut.ChannelState = ChannelState.Stopped;
            sut.ChannelState.ShouldBe(ChannelState.Stopped);
            channelStateChangingCalled.ShouldBeTrue();
        }

        [TestMethod]
        public void ChannelState_Started()
        {
            var channelStateChangingCalled = false;
            var sut = NewSut(ChannelNumber);
            sut.ChannelStateChanging += (sender, args) => { channelStateChangingCalled = true; };
            sut.ChannelState = ChannelState.Started;
            sut.ChannelState.ShouldBe(ChannelState.Started);
            channelStateChangingCalled.ShouldBeTrue();
            _channelProgressMock.Verify(x => x.Reset());
            _channelGraphMock.Verify(x => x.Reset(0, 0));
            _dateTimeServiceMock.Verify(x => x.Now);
        }

        private ChannelViewModel NewSut(int? channelNumber = null)
        {
            var sut = new ChannelViewModel(_channelInputMock.Object, _channelGraphMock.Object, _channelProgressMock.Object, _configServiceMock.Object, _dateTimeServiceMock.Object);
            if (channelNumber.HasValue)
            {
                _channelInputMock.SetupGet(x => x.AvailableSetups).Returns(_availableSetups);
                sut.Init(channelNumber.Value);
            }
            return sut;
        }
    }
}
