﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Dps5315LiIoCharger.ViewModels;
using Dps5315LiIoCharger.Services;
using Moq;
using Shouldly;
using System.Collections.Generic;

namespace Dps5315LiIoChargerTests.ViewModels
{
    [TestClass]
    public class ChannelInputViewModelTests
    {
        Mock<IFileService> _fileServiceMock;
        Mock<IMessagingService> _messagingServiceMock;

        [TestInitialize]
        public void TestInitialize()
        {
            _fileServiceMock = new Mock<IFileService>();
            _messagingServiceMock = new Mock<IMessagingService>();

            _fileServiceMock.Setup(x => x.GetChannelSetupNames()).Returns(new List<string> { "Setup 1", "Setup 2", "Setup 3" });
        }

        [TestMethod]
        public void Create()
        {
            Should.Throw<ArgumentNullException>(() => new ChannelInputViewModel(null, null));
            Should.Throw<ArgumentNullException>(() => new ChannelInputViewModel(_fileServiceMock.Object, null));
            var sut = NewSut();
            sut.ShouldNotBeNull();
        }

        [TestMethod]
        public void InitialSetup()
        {
            var sut = NewSut();

            sut.AvailableSetups.Count.ShouldBe(3);
            sut.SelectedSetupIndex = -1;
            sut.SkipConstantU.ShouldBe(false);
            sut.BeepWhenCharged.ShouldBe(false);
        }

        [TestMethod]
        public void ChargeVoltage()
        {
            var canChangeCalled = false;
            var sut = NewSut();
            Should.Throw<ArgumentException>(() => sut.ChargeVoltage = -1);
            Should.Throw<ArgumentException>(() => sut.ChargeVoltage = ChannelInputViewModel.MaxSeriesVoltage + 1);

            sut.PropertyCanChange += (sender, args) => { canChangeCalled = true; };
            sut.ChargeVoltage = ChannelInputViewModel.MaxMasterSlaveVoltage;
            sut.ChargeVoltage.ShouldBe(ChannelInputViewModel.MaxMasterSlaveVoltage);
            canChangeCalled.ShouldBe(true);

            sut.PreCutoutVoltage = ChannelInputViewModel.MaxMasterSlaveVoltage - 1;
            sut.ChargeVoltage = ChannelInputViewModel.MaxMasterSlaveVoltage - 2;
            sut.PreCutoutVoltage.ShouldBe(0);
            sut.MaxPreCutoutVoltage.ShouldBe(sut.ChargeVoltage);
        }

        [TestMethod]
        public void ChargeCurrent()
        {
            var sut = NewSut();
            Should.Throw<ArgumentException>(() => sut.ChargeCurrent = -1);
            Should.Throw<ArgumentException>(() => sut.ChargeCurrent = ChannelInputViewModel.MaxCurrent + 1);

            sut.ChargeCurrent = ChannelInputViewModel.MaxCurrent;
            sut.ChargeCurrent.ShouldBe(ChannelInputViewModel.MaxCurrent);
            sut.PreCurrent = ChannelInputViewModel.MaxCurrent - 1;
            sut.ChargeCutoutCurrent = ChannelInputViewModel.MaxCurrent - 1;
            sut.ChargeCurrent = ChannelInputViewModel.MaxCurrent - 2;
            sut.PreCurrent.ShouldBe(0);
            sut.ChargeCutoutCurrent.ShouldBe(0);
        }

        [TestMethod]
        public void PreCutoutVoltage()
        {
            var sut = NewSut();
            sut.ChargeVoltage = ChannelInputViewModel.MaxSeriesVoltage;
            Should.Throw<ArgumentException>(() => sut.PreCutoutVoltage = -1);
            Should.Throw<ArgumentException>(() => sut.PreCutoutVoltage = ChannelInputViewModel.MaxSeriesVoltage);
            sut.PreCutoutVoltage = ChannelInputViewModel.MaxSeriesVoltage - 1;
            sut.PreCutoutVoltage.ShouldBe(ChannelInputViewModel.MaxSeriesVoltage - 1);
        }

        [TestMethod]
        public void PreCurrent()
        {
            var sut = NewSut();
            sut.ChargeCurrent = ChannelInputViewModel.MaxCurrent;
            Should.Throw<ArgumentException>(() => sut.PreCurrent = -1);
            Should.Throw<ArgumentException>(() => sut.PreCurrent = ChannelInputViewModel.MaxCurrent);
            sut.PreCurrent = ChannelInputViewModel.MaxCurrent - 1;
            sut.PreCurrent.ShouldBe(ChannelInputViewModel.MaxCurrent - 1);
        }

        [TestMethod]
        public void ChargeCutoutCurrent()
        {
            var sut = NewSut();
            sut.ChargeCurrent = ChannelInputViewModel.MaxCurrent;
            Should.Throw<ArgumentException>(() => sut.ChargeCutoutCurrent = -1);
            Should.Throw<ArgumentException>(() => sut.ChargeCutoutCurrent = ChannelInputViewModel.MaxCurrent);
            sut.ChargeCutoutCurrent = ChannelInputViewModel.MaxCurrent - 1;
            sut.ChargeCutoutCurrent.ShouldBe(ChannelInputViewModel.MaxCurrent - 1);
        }

        [TestMethod]
        public void SkipConstantU()
        {
            var sut = NewSut();
            sut.SkipConstantU = !sut.SkipConstantU;
            sut.SkipConstantU.ShouldBeTrue();
        }

        [TestMethod]
        public void BeepWhenCharged()
        {
            var sut = NewSut();
            sut.BeepWhenCharged = !sut.BeepWhenCharged;
            sut.BeepWhenCharged.ShouldBeTrue();
        }

        [TestMethod]
        public void MaxChargeVoltage()
        {
            var sut = NewSut();
            sut.MaxChargeVoltage.ShouldBe(ChannelInputViewModel.MaxSeriesVoltage);
        }

        private ChannelInputViewModel NewSut()
        {
            return new ChannelInputViewModel(_fileServiceMock.Object, _messagingServiceMock.Object);
        }
    }
}
