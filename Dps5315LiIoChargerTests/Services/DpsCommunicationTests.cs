﻿using Dps5315LiIoCharger.Dps5315;
using Dps5315LiIoCharger.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shouldly;
using System;

namespace Dps5315LiIoChargerTests.Services
{
    [TestClass]
    public class DpsCommunicationTests
    {
        Mock<ISerialService> serialServiceMock;
        Mock<IDpsResponseFactory> dpsResponseFactoryMock;

        [TestInitialize]
        public void TestInitialize()
        {
            serialServiceMock = new Mock<ISerialService>();
            dpsResponseFactoryMock = new Mock<IDpsResponseFactory>();
        }

        [TestMethod]
        public void Create()
        {
            Should.Throw<ArgumentNullException>(() => new DpsCommunication(null, null));
            Should.Throw<ArgumentNullException>(() => new DpsCommunication(serialServiceMock.Object, null));
            var sut = new DpsCommunication(serialServiceMock.Object, dpsResponseFactoryMock.Object);
            sut.ShouldNotBeNull();
        }

        //TODO
        //[TestMethod]
        //public void Write()
        //{
        //    //var connectRequestBytes = new byte[] { 0x02, 0x58, 0x0F, 0xD0, 0x03 };
        //    var connectRequestBytes = new byte[] { 0x58 };
        //    serialServiceMock.Setup(x => x.WriteFrame(It.IsAny<byte[]>())).Callback<byte[]>(data => 
        //    {
        //        data.ShouldNotBeNull();
        //        data.Length.ShouldBe(connectRequestBytes.Length);
        //        for (var i = 0; i < connectRequestBytes.Length; i++) data[i].ShouldBe(connectRequestBytes[i]);
        //    });

        //    var sut = GetSut();
        //    sut.Write(new ConnectRequest());
        //}

        //[TestMethod]
        //public void Read()
        //{
        //    //var infoResponseBytes = new byte[] { 0x02, 0x69, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x31, 0x94, 0xC9, 0x03 };
        //    var infoResponseBytes = new byte[] { 0x69, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x31 };
        //    serialServiceMock.Setup(x => x.ReadFrame()).Returns(infoResponseBytes);
        //    dpsResponseFactoryMock.Setup(x => x.Get<IInfoResponse>()).Returns(new InfoResponse());

        //    var sut = GetSut();
        //    var response = sut.Read<IInfoResponse>();
        //    response.ShouldNotBeNull();
        //}

        private DpsCommunication GetSut()
        {
            return new DpsCommunication(serialServiceMock.Object, dpsResponseFactoryMock.Object);
        }
    }
}
