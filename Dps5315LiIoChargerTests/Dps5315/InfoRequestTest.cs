﻿using Dps5315LiIoCharger.Dps5315;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Dps5315LiIoChargerTests.Dps5315
{
    [TestClass]
    public class InfoRequestTest
    {
        [TestMethod]
        public void Create()
        {
            var sut = new InfoRequest();
            sut.ShouldNotBeNull();
        }

        [TestMethod]
        public void ToBytes()
        {
            var sut = new InfoRequest();
            var bytes = sut.ToBytes();
            bytes.ShouldNotBeNull();
            bytes.Length.ShouldBe(1);
            bytes[0].ShouldBe((byte)'I');
        }
    }
}
