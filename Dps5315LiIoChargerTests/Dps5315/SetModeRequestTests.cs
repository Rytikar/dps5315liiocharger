﻿using Dps5315LiIoCharger.Dps5315;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace Dps5315LiIoChargerTests.Dps5315
{
    [TestClass]
    public class SetModeRequestTests
    {
        const DpsMode mode = DpsMode.MasterSlave;

        [TestMethod]
        public void Create()
        {
            var sut = new SetModeRequest(mode);

            sut.ShouldNotBeNull();
            sut.Mode.ShouldBe(mode);
        }

        [TestMethod]
        public void ToBytes()
        {
            var sut = new SetModeRequest(mode);
            var bytes = sut.ToBytes();
            bytes.ShouldNotBeNull();
            bytes.Length.ShouldBe(2);
            bytes[0].ShouldBe((byte)'N');
            bytes[1].ShouldBe((byte)0x1F);
        }
    }
}
