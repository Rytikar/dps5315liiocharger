﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dps5315LiIoCharger.Dps5315;
using Shouldly;

namespace Dps5315LiIoChargerTests.Dps5315
{
    [TestClass]
    public class ConnectRequestTests
    {
        [TestMethod]
        public void Create()
        {
            var sut = new ConnectRequest();
            sut.ShouldNotBeNull();
        }

        [TestMethod]
        public void ToBytes()
        {
            var sut = new ConnectRequest();
            var bytes = sut.ToBytes();
            bytes.ShouldNotBeNull();
            bytes.Length.ShouldBe(1);
            bytes[0].ShouldBe((byte)'X');
        }
    }
}
