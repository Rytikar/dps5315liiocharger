﻿using Dps5315LiIoCharger.Dps5315;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;
using System;

namespace Dps5315LiIoChargerTests.Dps5315
{
    [TestClass]
    public class ConnectResponseTests
    {
        [TestMethod]
        public void Create()
        {
            var sut = new ConnectResponse();
            sut.ShouldNotBeNull();
            sut.HasCrc.ShouldBe(false);
        }

        [TestMethod]
        public void Decorate()
        {
            var sut = new ConnectResponse();
            Should.Throw<ArgumentException>(() => sut.Decorate(new byte[3] { 1,2,3 }));
            Should.Throw<ArgumentException>(() => sut.Decorate(new byte[1] { 7 }));
            sut.Decorate(new byte[1] { 6 });
        }
    }
}
