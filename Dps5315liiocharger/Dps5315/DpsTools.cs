﻿using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Tools;
using System;
using System.Collections.Generic;

namespace Dps5315LiIoCharger.Dps5315
{
    public static class DpsTools
    {
        private const byte ModeBitmask = 0x03;
        private const byte StatusBitmask = 0x0C;

        private const byte ModeSeries = 0;
        private const byte ModeDual = 1;
        private const byte ModeMasterSlave = 3;
        private const byte StatusMasterSlaveActive = 0;
        private const byte StatusMasterActive = 4;
        private const byte StatusSlaveActive = 8;
        private const byte StatusAllStandby = 12;
        private const byte RemoteActive = 16;

        private const double VoltageDivider = 100;
        private const double AmperageDivider = 1000;

        public static DpsMode ByteToDpsMode(byte b)
        {
            var mode = b & ModeBitmask;
            var status = b & StatusBitmask;
            LogTools.Status(new { b, mode, status });
            switch (mode)
            {
                case ModeSeries: return GetSeriesMode(status);
                case ModeMasterSlave: return GetMasterSlaveMode(status);
                default: throw new InvalidOperationException($"Invalid Mode: {mode}");
            }
        }

        public static byte DpsModeToByte(DpsMode mode, bool remote)
        {
            byte b = remote ? RemoteActive : (byte)0;
            switch (mode)
            {
                case DpsMode.MasterSlaveInActive: b |= (ModeMasterSlave | StatusAllStandby); break;
                case DpsMode.Master: b |= (ModeMasterSlave | StatusMasterActive); break;
                case DpsMode.Slave: b |= (ModeMasterSlave | StatusSlaveActive); break;
                case DpsMode.MasterSlave: b |= (ModeMasterSlave | StatusMasterSlaveActive); break;
                case DpsMode.Series: b |= (ModeSeries | StatusMasterSlaveActive); break;
                case DpsMode.SeriesInActive: b |= (ModeSeries | StatusAllStandby); break;
                default: throw new ArgumentOutOfRangeException($"Unknow value: {mode}", nameof(mode));
            }
            return b;
        }

        public static MasterSlaveVoltageCurrentContainer MasterSlaveVoltageCurrentContainerFromBytes(IList<byte> data, int index)
        {
            const int dataLength = 8;
            if (data.Count < index + dataLength) throw new ArgumentException($"Byte array has wrong length! Should be {index + dataLength}, is {data.Count}");
            MasterSlaveVoltageCurrentContainer result;
            result.Master.Voltage = BytesToVoltage(data[index + 0], data[index + 1]);
            result.Master.Current = BytesToCurrent(data[index + 2], data[index + 3]);
            result.Slave.Voltage = BytesToVoltage(data[index + 4], data[index + 5]);
            result.Slave.Current = BytesToCurrent(data[index + 6], data[index + 7]);
            return result;
        }

        public static double BytesToVoltage(byte high, byte low)
        {
            return ((high << 8) + low) / VoltageDivider;
        }

        public static double BytesToCurrent(byte high, byte low)
        {
            return ((high << 8) + low) / AmperageDivider;
        }

        public static byte[] VoltageToBytes(double voltage)
        {
            return ValueToBytes(voltage * VoltageDivider);
        }

        public static byte[] CurrentToBytes(double voltage)
        {
            return ValueToBytes(voltage * AmperageDivider);
        }

        private static byte[] ValueToBytes(double value)
        {
            var trunc = Math.Truncate(value);
            var us = trunc > ushort.MaxValue ? ushort.MaxValue : Convert.ToUInt16(trunc);
            return new[] { (byte)((us >> 8) & byte.MaxValue), (byte)(us & byte.MaxValue) };
        }

        private static DpsMode GetMasterSlaveMode(int status)
        {
            switch (status)
            {
                case StatusMasterSlaveActive: return DpsMode.MasterSlave;
                case StatusMasterActive: return DpsMode.Master;
                case StatusSlaveActive: return DpsMode.Slave;
                case StatusAllStandby: return DpsMode.MasterSlaveInActive;
                default: throw new InvalidOperationException($"Invalid Status: {status}");
            }
        }

        private static DpsMode GetSeriesMode(int status)
        {
            switch (status)
            {
                case StatusMasterSlaveActive: return DpsMode.Series;
                case StatusAllStandby: return DpsMode.SeriesInActive;
                default: throw new InvalidOperationException($"Invalid Status: {status}");
            }
        }
    }
}
