﻿namespace Dps5315LiIoCharger.Dps5315
{
    public class SetMasterSlaveLimitsRequest : SetLimitsRequest
    {
        public SetMasterSlaveLimitsRequest(double masterLimitVoltage, double masterLimitCurrent, double slaveLimitVoltage, double slaveLimitCurrent) : base(masterLimitVoltage, masterLimitCurrent, slaveLimitVoltage, slaveLimitCurrent)
        {}
    }
}
