﻿namespace Dps5315LiIoCharger.Dps5315
{
    public class InfoRequest : DpsRequest
    {
        public override byte[] ToBytes()
        {
            return new[] { (byte)'I' };
        }
    }
}
