﻿using Dps5315LiIoCharger.DataObjects;

namespace Dps5315LiIoCharger.Dps5315
{
    public interface ISeriesResponse : IDpsResponse
    {
        MasterSlaveVoltageCurrentContainer ActualUI { get; }
    }
}
