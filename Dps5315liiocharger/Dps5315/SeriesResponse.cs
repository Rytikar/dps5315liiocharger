﻿using Dps5315LiIoCharger.DataObjects;
using System.Collections.Generic;

namespace Dps5315LiIoCharger.Dps5315
{
    public class SeriesResponse : DpsResponse , ISeriesResponse
    {
        private const byte ResponseIdentifier = (byte)'s';
        public MasterSlaveVoltageCurrentContainer ActualUI { get; set; }
        public override int Length => 9;
        public override bool HasCrc => true;

        public override void Decorate(IList<byte> data)
        {
            DecorateCheck(data, ResponseIdentifier);
            ActualUI = DpsTools.MasterSlaveVoltageCurrentContainerFromBytes(data, 1);
        }
    }
}
