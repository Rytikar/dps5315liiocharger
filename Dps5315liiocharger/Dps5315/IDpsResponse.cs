﻿using System.Collections.Generic;

namespace Dps5315LiIoCharger.Dps5315
{
    public interface IDpsResponse
    {
        int Length { get; }
        bool HasCrc { get; }
        void Decorate(IList<byte> data);
    }
}
