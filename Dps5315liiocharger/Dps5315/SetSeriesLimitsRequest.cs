﻿using System;

namespace Dps5315LiIoCharger.Dps5315
{
    public class SetSeriesLimitsRequest : SetLimitsRequest
    {
        public SetSeriesLimitsRequest(double limitVoltage, double limitCurrent) : base(limitVoltage / 2, limitCurrent, limitVoltage / 2, limitCurrent)
        {}
    }
}
