﻿using System;
using System.Collections.Generic;

namespace Dps5315LiIoCharger.Dps5315
{
    public abstract class DpsResponse : IDpsResponse
    {
        public abstract int Length { get; }
        public abstract bool HasCrc { get; }
        public abstract void Decorate(IList<byte> data);

        protected void DecorateCheck(IList<byte> data, byte responseIdentifier)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));
            if (data.Count != Length) throw new ArgumentException($"Data length is invalid. Should be {Length}, is {data.Count}", nameof(data));
            if (data[0] != responseIdentifier) throw new ArgumentException($"Response identifier in data is invalid. Should be {responseIdentifier}, is {data[0]}", nameof(data));
        }
    }
}
