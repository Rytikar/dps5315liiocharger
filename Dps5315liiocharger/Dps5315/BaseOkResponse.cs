﻿using System.Collections.Generic;

namespace Dps5315LiIoCharger.Dps5315
{
    public abstract class BaseOkResponse : DpsResponse
    {
        private const byte ResponseIdentifier = 6;

        public override int Length => 1;

        public override bool HasCrc => false;

        public override void Decorate(IList<byte> data)
        {
            DecorateCheck(data, ResponseIdentifier);
        }
    }
}