﻿using Dps5315LiIoCharger.DataObjects;

namespace Dps5315LiIoCharger.Dps5315
{
    public interface IInfoResponse : IDpsResponse
    {
        DpsMode Mode { get; }
        MasterSlaveVoltageCurrentContainer ActualUI { get; }
        TemperatureInformation TemperatureInformation { get; }
    }
}
