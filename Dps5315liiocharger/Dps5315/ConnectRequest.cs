﻿namespace Dps5315LiIoCharger.Dps5315
{
    public class ConnectRequest : DpsRequest
    {
        public override byte[] ToBytes()
        {
            return new[] { (byte)'X' };
        }
    }
}
