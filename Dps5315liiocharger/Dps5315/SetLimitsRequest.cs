﻿using Dps5315LiIoCharger.DataObjects;
using System;

namespace Dps5315LiIoCharger.Dps5315
{
    public abstract class SetLimitsRequest : DpsRequest
    {
        public MasterSlaveVoltageCurrentContainer LimitsUI;

        protected SetLimitsRequest(double masterLimitVoltage, double masterLimitCurrent, double slaveLimitVoltage, double slaveLimitCurrent)
        {
            LimitsUI.Master.Voltage = masterLimitVoltage;
            LimitsUI.Master.Current = masterLimitCurrent;
            LimitsUI.Slave.Voltage = slaveLimitVoltage;
            LimitsUI.Slave.Current = slaveLimitCurrent;
        }

        public override byte[] ToBytes()
        {
            var result = new byte[9];
            result[0]= (byte)'T';
            Array.Copy(DpsTools.VoltageToBytes(LimitsUI.Master.Voltage), 0, result, 1, 2);
            Array.Copy(DpsTools.CurrentToBytes(LimitsUI.Master.Current), 0, result, 3, 2);
            Array.Copy(DpsTools.VoltageToBytes(LimitsUI.Slave.Voltage), 0, result, 5, 2);
            Array.Copy(DpsTools.CurrentToBytes(LimitsUI.Slave.Current), 0, result, 7, 2);
            return result;
        }
    }
}
