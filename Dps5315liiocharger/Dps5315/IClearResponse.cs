﻿using Dps5315LiIoCharger.DataObjects;

namespace Dps5315LiIoCharger.Dps5315
{
    public interface IClearResponse : IDpsResponse
    {
        DpsMode Mode { get; }
        MasterSlaveVoltageCurrentContainer ActualUI { get; }
    }
}
