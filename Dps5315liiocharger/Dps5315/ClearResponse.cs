﻿using Dps5315LiIoCharger.DataObjects;
using System.Collections.Generic;

namespace Dps5315LiIoCharger.Dps5315
{
    public class ClearResponse : DpsResponse, IClearResponse
    {
        private const byte ResponseIdentifier = (byte)'c';

        public DpsMode Mode { get; set; }
        public MasterSlaveVoltageCurrentContainer ActualUI { get; set; }

        public override int Length => 10;
        public override bool HasCrc => true;

        public override void Decorate(IList<byte> data)
        {
            DecorateCheck(data, ResponseIdentifier);
            Mode = DpsTools.ByteToDpsMode(data[1]);
            ActualUI = DpsTools.MasterSlaveVoltageCurrentContainerFromBytes(data, 2);
        }
    }
}
