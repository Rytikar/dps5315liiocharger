﻿using Dps5315LiIoCharger.DataObjects;
using System.Collections.Generic;

namespace Dps5315LiIoCharger.Dps5315
{
    public class InfoResponse : DpsResponse, IInfoResponse
    {
        private const byte ResponseIdentifier = (byte)'i';

        public DpsMode Mode { get; set; }
        public MasterSlaveVoltageCurrentContainer ActualUI { get; set; }
        public TemperatureInformation TemperatureInformation { get; set; }

        public override int Length => 13;
        public override bool HasCrc => true;

        public override void Decorate(IList<byte> data)
        {
            //TODO rdh check if not remote...
            DecorateCheck(data, ResponseIdentifier);

            Mode = DpsTools.ByteToDpsMode(data[1]);
            ActualUI = DpsTools.MasterSlaveVoltageCurrentContainerFromBytes(data, 3);
            TemperatureInformation = new TemperatureInformation(data[11], data[12], false, false);
        }
    }
}
