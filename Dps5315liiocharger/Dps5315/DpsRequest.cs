﻿namespace Dps5315LiIoCharger.Dps5315
{
    public abstract class DpsRequest : IDpsRequest
    {
        public abstract byte[] ToBytes();
    }
}
