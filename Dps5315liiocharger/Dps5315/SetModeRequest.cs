﻿namespace Dps5315LiIoCharger.Dps5315
{
    public class SetModeRequest : DpsRequest
    {
        public DpsMode Mode { get; }
        public bool Remote { get; }

        public SetModeRequest(DpsMode mode, bool remote = true)
        {
            Mode = mode;
            Remote = remote;
        }

        public override byte[] ToBytes()
        {
            return new[] { (byte)'N', DpsTools.DpsModeToByte(Mode, Remote) };
        }
    }
}
