﻿namespace Dps5315LiIoCharger.Dps5315
{
    public class ClearRequest : DpsRequest
    {
        public override byte[] ToBytes()
        {
            return new[] { (byte)'C' };
        }
    }
}