﻿namespace Dps5315LiIoCharger.Dps5315
{
    public class SeriesRequest : DpsRequest
    {
        public override byte[] ToBytes()
        {
            return new[] { (byte)'S' };
        }
    }
}
