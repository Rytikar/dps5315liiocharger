﻿using System.Windows;
using Dps5315LiIoCharger.ViewModels;

namespace Dps5315LiIoCharger.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContextChanged += MainWindow_DataContextChanged;
        }

        private void MainWindow_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var vm = DataContext as IMainWindowViewModel;
            if (vm == null) return;
            vm.MasterChannel.SynchronizationContext = vm.SynchronizationContext;
            vm.SlaveChannel.SynchronizationContext = vm.SynchronizationContext;
            MasterChannel.DataContext = vm.MasterChannel;
            SlaveChannel.DataContext = vm.SlaveChannel;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var vm = DataContext as IMainWindowViewModel;
            if (vm == null) return;
            vm.Closing();
        }
    }
}
