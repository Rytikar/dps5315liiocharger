﻿using System.Windows;
using Dps5315LiIoCharger.ViewModels;

namespace Dps5315LiIoCharger.Views
{
    /// <summary>
    /// Interaction logic for ChannelControl.xaml
    /// </summary>
    public partial class ChannelControl
    {
        public ChannelControl()
        {
            InitializeComponent();
            DataContextChanged += ChannelControl_DataContextChanged;
        }

        private void ChannelControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var vm = DataContext as IChannelViewModel;
            if (vm == null) return;
            vm.ChannelInput.SynchronizationContext = vm.SynchronizationContext;
            vm.ChannelGraph.SynchronizationContext = vm.SynchronizationContext;
            vm.ChannelProgress.SynchronizationContext = vm.SynchronizationContext;
            ChannelInput.DataContext = vm.ChannelInput;
            ChannelGraph.DataContext = vm.ChannelGraph;
            ChannelProgress.DataContext = vm.ChannelProgress;
        }
    }
}
