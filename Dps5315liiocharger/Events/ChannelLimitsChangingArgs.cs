﻿namespace Dps5315LiIoCharger.Events
{
    public class ChannelLimitsChangingArgs
    {
        public double Voltage { get; }
        public double Current { get; }
        public bool CanChange { get; set; }

        public ChannelLimitsChangingArgs(double voltage, double current)
        {
            CanChange = true;
            Voltage = voltage;
            Current = current;
        }

        public override string ToString()
        {
            return $"Voltage: {Voltage}, Current: {Current}, CanChange: {CanChange}";
        }
    }
}
