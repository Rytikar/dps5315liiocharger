﻿using System;

namespace Dps5315LiIoCharger.Events
{
    public class DisplayStatusMessageArgs : EventArgs
    {
        public string Message { get; set; }
    }
}
