﻿using Dps5315LiIoCharger.DataObjects;

namespace Dps5315LiIoCharger.Events
{
    public class ChannelStateChangingArgs
    {
        public ChannelState State { get; }
        public bool CanChange { get; set; }

        public ChannelStateChangingArgs(ChannelState state)
        {
            State = state;
            CanChange = true;
        }

        public override string ToString()
        {
            return $"State: {State}, CanChange: {CanChange}";
        }
    }
}
