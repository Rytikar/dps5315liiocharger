﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Dps5315LiIoCharger.Converter
{
    public class DoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double d;
            return double.TryParse((string)value, out d) ? d : 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((double)value).ToString();
        }
    }
}
