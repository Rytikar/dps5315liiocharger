﻿using System;
using Infralution.Localization.Wpf;

namespace Dps5315LiIoCharger.Converter
{
    public class LocalizedEnumConverter : ResourceEnumConverter
    {
        public LocalizedEnumConverter(Type type) : base(type, Resources.Enums.ResourceManager)
        {
        }
    }
}
