﻿using Dps5315LiIoCharger.Dps5315;
using Dps5315LiIoCharger.Services;
using Dps5315LiIoCharger.ViewModels;
using Dps5315LiIoCharger.Views;
using Microsoft.Practices.Unity;
using RJCP.IO.Ports;
using System;
using System.Windows;
using System.Windows.Threading;
using Dps5315LiIoCharger.Tools;
using System.IO;
using Infralution.Localization.Wpf;

[assembly: log4net.Config.XmlConfigurator()]
namespace Dps5315LiIoCharger
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomainUnhandledException; 
            DispatcherUnhandledException += AppDispatcherUnhandledException;
            LogTools.Enter();
            var container = InitialiseServices(new UnityContainer());

            //var configurationService = container.Resolve<IConfigService>();
            //CultureManager.UICulture = new System.Globalization.CultureInfo(configurationService.Configuration.SelectedCulture);

            var viewFactory = RegisterViews(container.Resolve<IViewFactory>()); 
            MainWindow = viewFactory.Resolve<IMainWindowViewModel>();
            MainWindow.Show();
            LogTools.Exit();
        }

        private void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = e.ExceptionObject as Exception;
            if (exception != null) LogTools.Error("Unhandled exception in application", exception);
            else LogTools.Error("Unhandled error in application");
        }

        private void AppDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            LogTools.Error("Unhandled exception in application", e.Exception);
            e.Handled = true;
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            LogTools.Enter();
            LogTools.Exit();
        }

        private static IUnityContainer InitialiseServices(IUnityContainer container)
        {
            LogTools.Enter();

            container.RegisterType<SerialPortStream, SerialPortStream>(new InjectionFactory(c => new SerialPortStream()));
            container.RegisterType<FileSystemWatcher, FileSystemWatcher>(new InjectionFactory(c => new FileSystemWatcher()));

            container.RegisterType<IViewModelFactory, ViewModelFactory>();
            container.RegisterType<IViewFactory, ViewFactory>();
            container.RegisterType<IDpsResponseFactory, DpsResponseFactory>();

            container.RegisterType<IDpsCommunication, DpsCommunicationEmulator>();
            //container.RegisterType<IDpsCommunication, DpsCommunication>();

            container.RegisterType<ISerialService, RjcpSerialService>();
            //container.RegisterType<ISerialService, MsSerialService>();

            container.RegisterType<IDateTimeService, DateTimeService>();
            container.RegisterType<ICrcService, CrcService>();
            container.RegisterType<IFileService, FileService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IConfigService, ConfigService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IMessagingService, MessagingService>(new ContainerControlledLifetimeManager());

            container.RegisterType<IConnectResponse, ConnectResponse>();
            container.RegisterType<ISetModeResponse, SetModeResponse>();
            container.RegisterType<IInfoResponse, InfoResponse>();

            container.RegisterType<IMainWindowViewModel, MainWindowViewModel>();
            container.RegisterType<IChannelInputViewModel, ChannelInputViewModel>();
            container.RegisterType<IChannelGraphViewModel, ChannelGraphViewModel>();
            container.RegisterType<IChannelProgressViewModel, ChannelProgressViewModel>();
            container.RegisterType<IChannelViewModel, ChannelViewModel>();

            container.RegisterType<MainWindow, MainWindow>();

            LogTools.Exit();

            return container;
        }

        private static IViewFactory RegisterViews(IViewFactory viewFactory)
        {
            viewFactory.Register<IMainWindowViewModel, MainWindow>();

            return viewFactory;
        }
    }
}
