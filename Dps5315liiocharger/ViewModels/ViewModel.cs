﻿using Dps5315LiIoCharger.Tools;
using System.Threading;

namespace Dps5315LiIoCharger.ViewModels
{
    public class ViewModel : NotifyableBase, IViewModel
    {
        private bool _isEnabled = true;

        public SynchronizationContext SynchronizationContext { get; set; }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { SetProperty(ref _isEnabled, value); }
        }
    }
}
