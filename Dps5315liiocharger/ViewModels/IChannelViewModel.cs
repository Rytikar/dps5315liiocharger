﻿using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Events;
using Dps5315LiIoCharger.Tools;
using System;

namespace Dps5315LiIoCharger.ViewModels
{
    public interface IChannelViewModel : IViewModel
    {
        void Init(int channelNumber);
        bool IsChannelConnected { get; set; }
        ChannelState ChannelState { get; set; }
        IChannelInputViewModel ChannelInput { get; }
        IChannelGraphViewModel ChannelGraph { get; }
        IChannelProgressViewModel ChannelProgress { get; }
        IRelayCommand StartStopCommand { get; }
        void ChargeProgress(double voltage, double current);
        event EventHandler<ChannelStateChangingArgs> ChannelStateChanging;
        event EventHandler<ChannelLimitsChangingArgs> ChannelLimitsChanging;
        string StartDisplayText { get; }
        double ChargingVoltage { get; }
        double ChargingCurrent { get; }
    }
}
