﻿using Dps5315LiIoCharger.Converter;
using System.ComponentModel;

namespace Dps5315LiIoCharger.ViewModels
{
    [TypeConverter(typeof(LocalizedEnumConverter))]
    public enum ChargingState { Stopped, Pre, Cc, Cv }
}
