﻿using OxyPlot;
using OxyPlot.Axes;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Dps5315LiIoCharger.ViewModels
{
    public class VoltageCurrentPoints
    {
        public ObservableCollection<DataPoint> Voltages { get; } = new ObservableCollection<DataPoint>();
        public ObservableCollection<DataPoint> Currents { get; } = new ObservableCollection<DataPoint>();
        public double MaxTime { get; private set; }
        public double MaxVoltage { get; private set; }
        public double MaxCurrent { get; private set; }

        public void Add(TimeSpan timeSpan, double voltage, double current)
        {
            Add(TimeSpanAxis.ToDouble(timeSpan), voltage, current);
        }

        public void Add(double timeSpan, double voltage, double current)
        {
            Voltages.Add(new DataPoint(timeSpan, voltage));
            Currents.Add(new DataPoint(timeSpan, current));
            if (timeSpan > MaxTime) MaxTime = timeSpan;
            if (voltage > MaxVoltage) MaxVoltage = voltage;
            if (current > MaxCurrent) MaxCurrent = current;
        }

        public void Clear()
        {
            Voltages.Clear();
            Currents.Clear();
            MaxTime = 0;
            MaxVoltage = 0;
            MaxCurrent = 0;
        }

        public bool Any => Voltages.Any();
        public int Count => Voltages.Count;
    }
}
