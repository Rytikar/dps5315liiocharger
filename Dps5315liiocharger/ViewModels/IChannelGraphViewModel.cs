﻿using Dps5315LiIoCharger.Tools;
using OxyPlot;
using System;

namespace Dps5315LiIoCharger.ViewModels
{
    public interface IChannelGraphViewModel : IViewModel
    {
        LegendPosition SelectedLegendPosition { get; set; }
        int TimeAxisMinimum { get; }
        int TimeAxisMaximum { get; }
        void Reset(double voltageLimit, double currentLimit);
        void Update(TimeSpan timeSpan, double voltage, double current);
        IRelayCommand SaveGraphCommand { get; }
        IRelayCommand LoadGraphCommand { get; }
        IRelayCommand ClearGraphCommand { get; }
    }
}
