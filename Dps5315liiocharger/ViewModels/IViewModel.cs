﻿using Dps5315LiIoCharger.Tools;
using System.Threading;

namespace Dps5315LiIoCharger.ViewModels
{
    public interface IViewModel : INotifyableBase
    {
        bool IsEnabled { get; set;}
        SynchronizationContext SynchronizationContext { get; set; }
    }
}
