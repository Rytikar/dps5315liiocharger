﻿using System.Collections.Generic;
using System.Globalization;
using Dps5315LiIoCharger.Tools;

namespace Dps5315LiIoCharger.ViewModels
{
    public interface IMainWindowViewModel : IViewModel
    {
        void Closing();
        bool IsConnected { get; }
        IList<CultureInfo> AvailableCultures { get; }
        CultureInfo SelectedCulture { get; set; }
        IChannelViewModel MasterChannel { get; }
        IChannelViewModel SlaveChannel { get; }
        IRelayCommand ConnectCommand { get; }
        string ConnectButtonDisplayText { get; }
        string ConnectStatusDisplayText { get; }
        string TemperatureDisplayText { get; }
        string DpsModeDisplayText { get; }
    }
}
