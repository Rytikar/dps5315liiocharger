﻿namespace Dps5315LiIoCharger.ViewModels
{
    public class WindowViewModel : ViewModel, IWindowViewModel
    {
        private string _title;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
    }
}
