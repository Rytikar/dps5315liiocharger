﻿namespace Dps5315LiIoCharger.ViewModels
{
    public interface IWindowViewModel : IViewModel
    {
        string Title { get; set; }
    }
}
