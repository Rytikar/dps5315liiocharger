﻿using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Dps5315LiIoCharger.ViewModels
{
    public interface IChannelInputViewModel : IViewModel
    {
        ObservableCollection<string> AvailableSetups { get; }
        int SelectedSetupIndex { get; set; }
        double PreCutoutVoltage { get; set; }
        double PreCurrent { get; set; }
        double PreCurrentPercent { get; }
        double ChargeVoltage { get; set; }
        double ChargeCurrent { get; set; }
        double ChargeCutoutCurrent { get; set; }
        double ChargeCutoutCurrentPercent { get; }
        double MaxChargeVoltage { get; }
        double MaxPreCutoutVoltage { get; }
        double MaxPreCurrent { get; }
        double MaxChargeCutoutCurrent { get; }
        bool SkipConstantU { get; }
        bool BeepWhenCharged { get; }
        ICommand SaveSetupCommand { get; }
    }
}
