﻿using System;

namespace Dps5315LiIoCharger.ViewModels
{
    public interface IChannelProgressViewModel : IViewModel
    {
        void Reset();
        void SetLimits(double voltage, double current, ChargingState chargingState);
        void Update(TimeSpan timeSpan, double voltage, double current);
        ChargingState ChargingState { get; }
        string ChargingStateDisplayText { get; }
        double ActualPower { get; }
        TimeSpan ChargingTime { get; }
        double LimitVoltage { get; }
        double LimitCurrent { get; }
        double ActualVoltage { get; }
        double ActualCurrent { get; }
        double ChargeApplied { get; }
    }
}
