﻿using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Services;
using Dps5315LiIoCharger.Tools;
using System;
using Dps5315LiIoCharger.Events;
using System.ComponentModel;
using System.Diagnostics;

namespace Dps5315LiIoCharger.ViewModels
{
    public class ChannelViewModel : ViewModel, IChannelViewModel
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IConfigService _configService;
        private ChannelState _channelState = ChannelState.Stopped;
        private Stopwatch _stopWatch = new Stopwatch();
        private bool _isChannelConnected;
        private int _channelNumber;

        public ChannelViewModel(
            IChannelInputViewModel channelInput, 
            IChannelGraphViewModel channelGraph, 
            IChannelProgressViewModel channelProgress,
            IConfigService configService,
            IDateTimeService dateTimeService)
        {
            if (channelInput == null) throw new ArgumentNullException(nameof(channelInput));
            if (channelGraph == null) throw new ArgumentNullException(nameof(channelGraph));
            if (channelProgress == null) throw new ArgumentNullException(nameof(channelProgress));
            if (configService == null) throw new ArgumentNullException(nameof(configService));
            if (dateTimeService == null) throw new ArgumentNullException(nameof(dateTimeService));

            _configService = configService;
            _dateTimeService = dateTimeService;

            ChannelInput = channelInput;
            ChannelGraph = channelGraph;
            ChannelProgress = channelProgress;

            ChannelInput.PropertyCanChange += ChannelInputPropertyCanChange;
            ChannelInput.PropertyChanged += ChannelInputPropertyChanged;
            ChannelGraph.PropertyChanged += ChannelGraphPropertyChanged;

            StartStopCommand = new RelayCommand(StartStopExecute, StartStopCanExecute);
        }

        public void Init(int channelNumber)
        {
            _channelNumber = channelNumber;
            ChannelInput.SelectedSetupIndex = ChannelInput.AvailableSetups.IndexOf(_configService.Configuration.LastSetups[_channelNumber]);
            ChannelGraph.SelectedLegendPosition = _configService.Configuration.LegendPositions[_channelNumber];
        }

        public IChannelInputViewModel ChannelInput { get; }
        public IChannelGraphViewModel ChannelGraph { get; }
        public IChannelProgressViewModel ChannelProgress { get; }
        public IRelayCommand StartStopCommand { get; }
        public event EventHandler<ChannelStateChangingArgs> ChannelStateChanging;
        public event EventHandler<ChannelLimitsChangingArgs> ChannelLimitsChanging;

        public string StartDisplayText => ChannelState == ChannelState.Stopped ? Resources.ChannelControl.StartStopCommandStart : Resources.ChannelControl.StartStopCommandStop;

        public double ChargingVoltage { get; private set; }
        public double ChargingCurrent { get; private set; }

        public bool IsChannelConnected
        {
            get { return _isChannelConnected; }
            set
            {
                if (!SetProperty(ref _isChannelConnected, value)) return;
                if (ChannelState != ChannelState.Stopped) StartStopCommand.Execute(null);
                RaiseCanExecuteChanged();
            }
        }

        public ChannelState ChannelState
        {
            get { return _channelState; }
            set
            {
                if (!RaiseChannelStateChanging(value)) return;
                if (!SetProperty(ref _channelState, value)) return;

                switch(ChannelState)
                {
                    case ChannelState.Started: StartChannel(); break;
                    case ChannelState.Stopped: StopChannel(); break;
                    case ChannelState.Paused: PauseChannel(); break;
                }

                OnPropertyChanged(nameof(StartDisplayText));

                RaiseCanExecuteChanged();
            }
        }

        private void StartChannel()
        {
            _stopWatch.Restart();
            ChannelProgress.Reset();
            ChannelGraph.Reset(ChannelInput.ChargeVoltage, ChannelInput.ChargeCurrent);
        }

        private void StopChannel()
        {
            _stopWatch.Reset();
        }

        private void PauseChannel()
        {
            _stopWatch.Stop();
        }

        public void ChargeProgress(double voltage, double current)
        {
            LogTools.Status($"U:{voltage}, I:{current}");

            var timeSpan = _stopWatch.Elapsed;
            ChannelProgress.Update(timeSpan, voltage, current);
            ChannelGraph.Update(timeSpan, voltage, current);

            EvaluateChargingProfile(voltage, current);
        }

        #region Commands

        protected void StartStopExecute(object obj)
        {
            ChannelState = ChannelState == ChannelState.Stopped ? ChannelState.Started : ChannelState.Stopped;
        }

        protected bool StartStopCanExecute(object obj)
        {
            return IsChannelConnected;
        }

        #endregion

        #region private

        private void ChannelInputPropertyCanChange(object sender, PropertyCanChangeEventArgs e)
        {
            if (e.PropertyName != nameof(IChannelInputViewModel.ChargeVoltage)) return;
            if (ChannelState == ChannelState.Stopped) return;
            if (RaiseChannelLimitsChanging((e as PropertyCanChangeEventArgs<double>).NewValue, ChannelInput.ChargeCurrent)) return;
            throw new InvalidOperationException(Resources.Errors.ErrorChargeVoltageChanging);  
        }

        private void ChannelInputPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(IChannelInputViewModel.SelectedSetupIndex)) return;
            if ((ChannelInput.SelectedSetupIndex < 0) || (ChannelInput.AvailableSetups.Count < ChannelInput.SelectedSetupIndex)) return;
            _configService.Configuration.SetLastSetup(_channelNumber, ChannelInput.AvailableSetups[ChannelInput.SelectedSetupIndex]);
        }

        private void ChannelGraphPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(IChannelGraphViewModel.SelectedLegendPosition)) return;
            _configService.Configuration.SetLegendPosition(_channelNumber, ChannelGraph.SelectedLegendPosition);
        }

        private void EvaluateChargingProfile(double voltage, double current)
        {
            LogTools.Enter(new { ChannelProgress.ChargingState, voltage, current, ChannelInput.ChargeVoltage, ChannelInput.ChargeCurrent, ChannelInput.PreCurrent, ChannelInput.PreCutoutVoltage, ChannelInput.ChargeCutoutCurrent });
            switch (ChannelProgress.ChargingState)
            {
                case ChargingState.Stopped:
                    {
                        var hasPre = (ChannelInput.PreCurrent > 0) && (ChannelInput.PreCutoutVoltage > 0);
                        ChangeLimits(ChannelInput.ChargeVoltage, hasPre ? ChannelInput.PreCurrent : ChannelInput.ChargeCurrent, hasPre ? ChargingState.Pre : ChargingState.Cc);
                        break;
                    }
                case ChargingState.Pre:
                    {
                        if (voltage < ChannelInput.PreCutoutVoltage) break;
                        ChangeLimits(ChannelInput.ChargeVoltage, ChannelInput.ChargeCurrent, ChargingState.Cc);
                        break;
                    }
                case ChargingState.Cc:
                    {
                        if (voltage < ChannelInput.ChargeVoltage) break;
                        if (ChannelInput.SkipConstantU) StopCharging();
                        else ChangeLimits(ChannelInput.ChargeVoltage, ChannelInput.ChargeCurrent, ChargingState.Cv);
                        break;
                    }
                case ChargingState.Cv:
                    {
                        if (current > ChannelInput.ChargeCutoutCurrent) break;
                        StopCharging();
                        break;
                    }
            }
            LogTools.Exit(new { ChannelProgress.ChargingState });
        }

        private void StopCharging()
        {
            ChangeLimits(0, 0, ChargingState.Stopped);
            StartStopCommand.Execute(null);
            if (ChannelInput.BeepWhenCharged) Console.Beep(1000, 1000);
        }

        private void ChangeLimits(double voltage, double current, ChargingState chargingState)
        {
            LogTools.Enter(new { voltage, current, chargingState});
            if (!RaiseChannelLimitsChanging(voltage, current))
            {
                LogTools.Exit("Not Changing");
                return;
            }
            ChargingVoltage = voltage;
            ChargingCurrent = current;
            LogTools.Status("Changing");
            ChannelProgress.SetLimits(ChargingVoltage, ChargingCurrent, chargingState);
            LogTools.Exit(new { voltage, current, chargingState });
        }

        private bool RaiseChannelLimitsChanging(double voltage, double current)
        {
            var handler = ChannelLimitsChanging;
            var e = new ChannelLimitsChangingArgs(voltage, current);
            handler?.Invoke(this, e);
            return e.CanChange;
        }

        private bool RaiseChannelStateChanging(ChannelState state)
        {
            var handler = ChannelStateChanging;
            var e = new ChannelStateChangingArgs(state);
            handler?.Invoke(this, e);
            return e.CanChange;
        }

        private void RaiseCanExecuteChanged()
        {
            StartStopCommand.RaiseCanExecuteChanged();
        }

        #endregion
    }
}
