﻿using Dps5315LiIoCharger.Tools;
using System;
using Infralution.Localization.Wpf;

namespace Dps5315LiIoCharger.ViewModels
{
    public class ChannelProgressViewModel : ViewModel, IChannelProgressViewModel
    {
        private double _limitCurrent;
        private double _limitVoltage;
        private double _actualCurrent;
        private double _actualVoltage;
        private TimeSpan _chargingTime;
        private double _chargeApplied;
        private ChargingState _chargingState;

        public string ChargingStateDisplayText => ResourceEnumConverter.ConvertToString(ChargingState);

        public ChargingState ChargingState
        {
            get { return _chargingState; }
            private set
            {
                if (!SetProperty(ref _chargingState, value)) return;
                OnPropertyChanged(nameof(ChargingStateDisplayText));
            }
        }

        public double ActualPower => ActualVoltage * ActualCurrent;

        public double LimitVoltage
        {
            get { return _limitVoltage; }
            set { SetProperty(ref _limitVoltage, value); }
        }

        public double LimitCurrent
        {
            get { return _limitCurrent; }
            set { SetProperty(ref _limitCurrent, value); }
        }

        public double ActualVoltage
        {
            get { return _actualVoltage; }
            set
            {
                if (!SetProperty(ref _actualVoltage, value)) return;
                OnPropertyChanged(nameof(ActualPower));
            }
        }

        public double ActualCurrent
        {
            get { return _actualCurrent; }
            set
            {
                if (!SetProperty(ref _actualCurrent, value)) return;
                OnPropertyChanged(nameof(ActualPower));
            }
        }

        public double ChargeApplied
        {
            get { return _chargeApplied; }
            set { SetProperty(ref _chargeApplied, value); }
        }

        public TimeSpan ChargingTime
        {
            get { return _chargingTime; }
            set { SetProperty(ref _chargingTime, value); }
        }

        public void Reset()
        {
            LimitVoltage = 0;
            LimitCurrent = 0;
            ActualVoltage = 0;
            ActualCurrent = 0;
            ChargeApplied = 0;
            ChargingState = ChargingState.Stopped;
            ChargingTime = TimeSpan.Zero;
        }

        public void SetLimits(double voltage, double current, ChargingState chargingState)
        {
            LimitVoltage = voltage;
            LimitCurrent = current;
            ChargingState = chargingState;
        }

        public void Update(TimeSpan timeSpan, double voltage, double current)
        {
            var logArgs = LogTools.Enter(new { voltage, current });

            ActualVoltage = voltage;
            ActualCurrent = current;
            ChargeApplied += CalcChargeApplied(timeSpan - ChargingTime, voltage, current);
            ChargingTime = timeSpan;

            LogTools.Exit(logArgs);
        }

        private double CalcChargeApplied(TimeSpan timeSpan, double voltage, double current)
        {
            const double MillisecondsInHour = 3600000;
            if (timeSpan.Milliseconds == 0) return 0;
            return (voltage * current) * (timeSpan.Milliseconds / MillisecondsInHour);
        }
    }
}
