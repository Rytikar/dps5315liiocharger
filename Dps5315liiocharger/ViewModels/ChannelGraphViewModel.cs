﻿using System;
using OxyPlot;
using Dps5315LiIoCharger.Tools;
using System.Collections.Generic;
using Dps5315LiIoCharger.Services;
using Dps5315LiIoCharger.Events;

namespace Dps5315LiIoCharger.ViewModels
{
    public class ChannelGraphViewModel : ViewModel, IChannelGraphViewModel
    {
        private const int DefaultTimeAxisMinimum = 0;
        private const int DefaultTimeAxisMaximum = 60;
        private const int TimeAxisMovement = 30;
        private const char ColumnSeparator = ';';
        private const double VoltageLimitMultiplier = 1.3;
        private const double CurrentLimitMultiplier = 1.1;

        private readonly IFileService _fileService;
        private readonly IMessagingService _messagingService;
        private int _timeAxisMinimum = DefaultTimeAxisMinimum;
        private int _timeAxisMaximum = DefaultTimeAxisMaximum;
        private double _voltageLimit;
        private double _currentLimit;
        private double _voltageSteps;
        private double _currentSteps;
        private LegendPosition _selectedLegendPosition;

        public VoltageCurrentPoints ChargingPoints { get; } = new VoltageCurrentPoints();
        public VoltageCurrentPoints LoadedPoints { get; } = new VoltageCurrentPoints();
        public IDictionary<LegendPosition, string> AvailableLegendPositions { get; } = new Dictionary<LegendPosition, string>();
        public IRelayCommand SaveGraphCommand { get; }
        public IRelayCommand LoadGraphCommand { get; }
        public IRelayCommand ClearGraphCommand { get; }

        public ChannelGraphViewModel(IFileService fileService, IMessagingService messagingService)
        {
            if (fileService == null) throw new ArgumentNullException(nameof(fileService));
            if (messagingService == null) throw new ArgumentNullException(nameof(messagingService));
            _fileService = fileService;
            _messagingService = messagingService;

            AvailableLegendPositions.Add(LegendPosition.TopLeft, Resources.ChannelGraphControl.LegendPositionTopLeft);
            AvailableLegendPositions.Add(LegendPosition.TopRight, Resources.ChannelGraphControl.LegendPositionTopRight);
            AvailableLegendPositions.Add(LegendPosition.BottomLeft, Resources.ChannelGraphControl.LegendPositionBottomLeft);
            AvailableLegendPositions.Add(LegendPosition.BottomRight, Resources.ChannelGraphControl.LegendPositionBottomRight);
            SaveGraphCommand = new RelayCommand(SaveGraphExecute, SaveGraphCanExecute);
            LoadGraphCommand = new RelayCommand(LoadGraphExecute);
            ClearGraphCommand = new RelayCommand(ClearGraphExecute, ClearGraphCanExecute);

            SetLimits();
        }

        public int TimeAxisMinimum
        {
            get { return _timeAxisMinimum; }
            private set { SetProperty(ref _timeAxisMinimum, value); }
        }

        public int TimeAxisMaximum
        {
            get { return _timeAxisMaximum; }
            private set { SetProperty(ref _timeAxisMaximum, value); }
        }

        public double VoltageLimit
        {
            get { return _voltageLimit; }
            private set { SetProperty(ref _voltageLimit, value); }
        }

        public double CurrentLimit
        {
            get { return _currentLimit; }
            private set { SetProperty(ref _currentLimit, value); }
        }

        public double VoltageSteps
        {
            get { return _voltageSteps; }
            private set { SetProperty(ref _voltageSteps, value); }
        }

        public double CurrentSteps
        {
            get { return _currentSteps; }
            private set { SetProperty(ref _currentSteps, value); }
        }

        public LegendPosition SelectedLegendPosition
        {
            get { return _selectedLegendPosition; }
            set { SetProperty(ref _selectedLegendPosition, value); }
        }

        public bool LoadPointsVisible => LoadedPoints.Any;

        public void Reset(double voltageLimit, double currentLimit)
        {
            LogTools.Enter();

            ChargingPoints.Clear();
            SetLimits();

            LogTools.Exit();
        }

        public void SetLimits()
        {
            var maxVoltage = Math.Max(ChargingPoints.MaxVoltage, LoadedPoints.MaxVoltage);
            var maxCurrent = Math.Max(ChargingPoints.MaxCurrent, LoadedPoints.MaxCurrent);
            var maxTime = Math.Max(ChargingPoints.MaxTime, LoadedPoints.MaxTime);

            VoltageLimit = Math.Max(2.0, Math.Round(maxVoltage * VoltageLimitMultiplier, 1));
            CurrentLimit = Math.Max(0.5, Math.Round(maxCurrent * CurrentLimitMultiplier, 1));
            VoltageSteps = VoltageLimit > 10 ? 5 : (VoltageLimit > 1 ? 1 : 0.1);
            CurrentSteps = CurrentLimit > 1 ? 0.5 : 0.1;

            TimeAxisMinimum = DefaultTimeAxisMinimum;
            TimeAxisMaximum = Math.Max((int)Math.Truncate(maxTime) + TimeAxisMovement, DefaultTimeAxisMaximum);

            RaiseCanExecuteChanged();
        }

        public void Update(TimeSpan timeSpan, double voltage, double current)
        {
            var logArgs = LogTools.Enter(new { timeSpan, voltage, current });

            ChargingPoints.Add(timeSpan, voltage, current);
            SetLimits();

            LogTools.Exit(logArgs);
        }

        #region Commands

        private void LoadGraphExecute(object obj)
        {
            LogTools.Enter();
            try
            {
                ClearGraphExecute(null);
                var fileName = _fileService.GetLoadGraphFileName();
                if (fileName == null) return;

                _fileService.ReadTextFile(fileName, (string line) =>
                {
                    if (string.IsNullOrEmpty(line)) return;
                    var split = line.Split(ColumnSeparator);
                    if (split.Length != 3) return;
                    double timeSpan, voltage, current;
                    if (!(Double.TryParse(split[0], out timeSpan) && Double.TryParse(split[1], out voltage) && Double.TryParse(split[2], out current))) return;
                    LoadedPoints.Add(timeSpan, voltage, current);
                });

                SetLimits();
            }
            catch (Exception exception)
            {
                _messagingService.Raise(this, new DisplayStatusMessageArgs { Message = LogTools.Error(exception) });
            }
            finally
            {
                LogTools.Exit();
            }
        }

        private bool ClearGraphCanExecute(object obj)
        {
            return ChargingPoints.Any || LoadedPoints.Any;
        }

        private void ClearGraphExecute(object obj)
        {
            ChargingPoints.Clear();
            LoadedPoints.Clear();
            SetLimits();
        }

        private bool SaveGraphCanExecute(object obj)
        {
            return ChargingPoints.Any;
        }

        private void SaveGraphExecute(object obj)
        {
            LogTools.Enter();
            try
            {
                if (!ChargingPoints.Any) return;
                var fileName = _fileService.GetSaveGraphFileName();
                if (fileName == null) return;
                _fileService.WriteTextFile(fileName, (int lineNr) =>
                {
                    if (lineNr == 0) return $"Time{ColumnSeparator}Volt{ColumnSeparator}Ampère";
                    if (lineNr >= ChargingPoints.Count) return null;
                    return $"{ChargingPoints.Voltages[lineNr].X}{ColumnSeparator}{ChargingPoints.Voltages[lineNr].Y}{ColumnSeparator}{ChargingPoints.Currents[lineNr].Y}";
                });
            }
            catch (Exception exception)
            {
                _messagingService.Raise(this, new DisplayStatusMessageArgs { Message = LogTools.Error(exception) });
            }
            finally
            {
                LogTools.Exit();
            }
        }

        private void RaiseCanExecuteChanged()
        {
            SaveGraphCommand.RaiseCanExecuteChanged();
            LoadGraphCommand.RaiseCanExecuteChanged();
            ClearGraphCommand.RaiseCanExecuteChanged();
            OnPropertyChanged(nameof(LoadPointsVisible));
        }

        #endregion
    }
}
