﻿using System;

namespace Dps5315LiIoCharger.ViewModels.Design
{
    public class ChannelProgressDesignViewModel : ViewModel, IChannelProgressViewModel
    {
        public ChannelProgressDesignViewModel()
        {
            ActualVoltage = 12.34;
            ActualCurrent = 1.23;
            ChargingTime = new TimeSpan(2, 14, 51);
            ChargeApplied = 2478.36;
        }

        public void Reset() {}
        public void SetLimits(double voltage, double current, ChargingState chargingState) {}
        public void Update(TimeSpan timeSpan, double voltage, double current){}
        public double ActualPower => ActualVoltage * ActualCurrent;
        public ChargingState ChargingState { get; }
        public string ChargingStateDisplayText => "Constant I";
        public TimeSpan ChargingTime { get; set; }
        public double LimitVoltage { get; set; }
        public double LimitCurrent { get; set; }
        public double ActualVoltage { get; set; }
        public double ActualCurrent { get; set; }
        public double ChargeApplied { get; set; }
    }
}
