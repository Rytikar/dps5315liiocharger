﻿using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Tools;
using System;
using Dps5315LiIoCharger.Events;

namespace Dps5315LiIoCharger.ViewModels.Design
{
    public class ChannelDesignViewModel : ViewModel, IChannelViewModel
    {
        public ChannelDesignViewModel()
        {
            //Dummy calls to get rid of warnings. Is obviously null...
            ChannelStateChanging?.Invoke(this, null);
            ChannelLimitsChanging?.Invoke(this, null);
        }

        public bool IsChannelConnected { get; set; }
        public ChannelState ChannelState { get; set; }
        public IChannelInputViewModel ChannelInput { get; }
        public IChannelGraphViewModel ChannelGraph { get; }
        public IChannelProgressViewModel ChannelProgress { get; }
        public IRelayCommand StartStopCommand { get; }
        public string StartDisplayText => "Start";
        public double ChargingVoltage { get; }
        public double ChargingCurrent { get; }

        public event EventHandler<ChannelStateChangingArgs> ChannelStateChanging;
        public event EventHandler<ChannelLimitsChangingArgs> ChannelLimitsChanging;

        public void ChargeProgress(double voltage, double current){}
        public void Init(int channelNumber){}
    }
}
