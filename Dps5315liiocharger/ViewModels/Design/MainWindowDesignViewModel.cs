﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Dps5315LiIoCharger.Tools;

namespace Dps5315LiIoCharger.ViewModels.Design
{
    public class MainWindowDesignViewModel : WindowViewModel, IMainWindowViewModel
    {
        public IList<CultureInfo> AvailableCultures { get; }
        public CultureInfo SelectedCulture { get; set; }
        public bool IsConnected { get; }
        public IRelayCommand ConnectCommand { get; }
        public IChannelViewModel MasterChannel { get; }
        public IChannelViewModel SlaveChannel { get; }
        public string ConnectButtonDisplayText { get; }
        public string ConnectStatusDisplayText { get; }
        public string TemperatureDisplayText { get; }
        public string DpsModeDisplayText { get; }
        public void Closing(){throw new NotImplementedException();}
    }
}
