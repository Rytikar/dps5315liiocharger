﻿using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Dps5315LiIoCharger.ViewModels.Design
{
    public class ChannelInputDesignViewModel : ViewModel, IChannelInputViewModel
    {
        public ChannelInputDesignViewModel()
        {
            AvailableSetups = new ObservableCollection<string> { "Setup 1", "Setup 2", "Setup 3" };
            MaxChargeVoltage = 30;
            SelectedSetupIndex = 0;
            ChargeVoltage = 4.2;
            ChargeCurrent = 1.5;
            PreCutoutVoltage = 2.5;
            PreCurrent = 0.1;
            ChargeCutoutCurrent = 0.15;
            SkipConstantU = true;
            BeepWhenCharged = true;
        }

        public ObservableCollection<string> AvailableSetups { get; private set; }
        public int SelectedSetupIndex { get; set; }
        public double PreCutoutVoltage { get; set; }
        public double PreCurrent { get; set; }
        public double ChargeCurrent { get; set; }
        public double ChargeVoltage { get; set; }
        public double MaxChargeVoltage { get; }
        public double ChargeCutoutCurrent { get; set; }

        public double PreCurrentPercent => (ChargeCurrent < 0.000001) ? 0 : (100.0 / ChargeCurrent) * PreCurrent;
        public double ChargeCutoutCurrentPercent => (ChargeCurrent < 0.000001) ? 0 : (100.0 / ChargeCurrent) * ChargeCutoutCurrent;
        public double MaxPreCutoutVoltage => ChargeVoltage;
        public double MaxPreCurrent => ChargeCurrent;
        public double MaxChargeCutoutCurrent => ChargeCurrent;
        public bool SkipConstantU { get; set; }
        public bool BeepWhenCharged { get; set; }

        public ICommand SaveSetupCommand { get; }
    }
}
