﻿using Dps5315LiIoCharger.Tools;
using OxyPlot;
using System;
using System.Collections.Generic;

namespace Dps5315LiIoCharger.ViewModels.Design
{
    public class ChannelGraphDesignViewModel : ViewModel, IChannelGraphViewModel
    {
        public ChannelGraphDesignViewModel()
        {
            TimeAxisMinimum = 0;
            TimeAxisMaximum = 30;
            VoltageLimit = 10;
            CurrentLimit = 3;

            ChargingPoints.Add(0, 24, 4);
            ChargingPoints.Add(10, 20, 8);
            ChargingPoints.Add(20, 16, 12);
            ChargingPoints.Add(30, 12, 16);
            ChargingPoints.Add(40, 8, 20);
            ChargingPoints.Add(50, 4, 24);
        }

        public VoltageCurrentPoints ChargingPoints { get; } = new VoltageCurrentPoints();
        public VoltageCurrentPoints LoadedPoints { get; } = new VoltageCurrentPoints();
        public int TimeAxisMinimum { get; }
        public int TimeAxisMaximum { get; }
        public int VoltageLimit { get; }
        public int CurrentLimit { get; }
        public LegendPosition SelectedLegendPosition { get; set; }
        public IDictionary<LegendPosition, string> AvailableLegendPositions { get; } = new Dictionary<LegendPosition, string>();
        public IRelayCommand SaveGraphCommand { get { throw new NotImplementedException(); } }
        public IRelayCommand LoadGraphCommand { get { throw new NotImplementedException(); } }
        public IRelayCommand ClearGraphCommand { get { throw new NotImplementedException(); } }
        public void Reset(double limitVoltage, double limitCurrent) {}
        public void Update(TimeSpan timeSpan, double voltage, double current) { }
    }
}
