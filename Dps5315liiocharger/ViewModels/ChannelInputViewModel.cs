﻿using System;
using System.Collections.ObjectModel;
using Dps5315LiIoCharger.Services;
using System.IO;
using Dps5315LiIoCharger.Tools;
using System.Windows.Input;
using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Events;

namespace Dps5315LiIoCharger.ViewModels
{
    public class ChannelInputViewModel : ViewModel, IChannelInputViewModel
    {
        public const int MaxMasterSlaveVoltage = 15;
        public const int MaxSeriesVoltage = 30;
        public const int MaxCurrent = 3;

        private readonly IFileService _fileService;
        private readonly IMessagingService _messagingService;
        private string _savedFileName;
        private int _selectedSetupIndex = -1;
        private double _preCutoutVoltage;
        private double _preCurrent;
        private double _chargeCurrent;
        private double _chargeVoltage;
        private double _chargeCutoutCurrent;
        private bool _skipConstantU;
        private bool _beepWhenCharged;

        public ChannelInputViewModel(IFileService fileService, IMessagingService messagingService)
        {
            if (fileService == null) throw new ArgumentNullException(nameof(fileService));
            if (messagingService == null) throw new ArgumentNullException(nameof(messagingService));
            _fileService = fileService;
            _messagingService = messagingService;

            SaveSetupCommand = new RelayCommand(SaveSetupExecute);

            SetupAvailableSetups();
            _fileService.SetupFilesChanged += SetupFilesChanged;
        }

        public ICommand SaveSetupCommand { get; }
        public ObservableCollection<string> AvailableSetups { get; } = new ObservableCollection<string>();
        public double MaxChargeVoltage => MaxSeriesVoltage;
        public double MaxPreCutoutVoltage => ChargeVoltage;
        public double MaxPreCurrent => ChargeCurrent;
        public double MaxChargeCutoutCurrent => ChargeCurrent;
        public double PreCurrentPercent => (ChargeCurrent < 0.000001) ? 0 : (100 / ChargeCurrent) * PreCurrent;
        public double ChargeCutoutCurrentPercent => (ChargeCurrent < 0.000001) ? 0 : (100 / ChargeCurrent) * ChargeCutoutCurrent;

        public double ChargeVoltage
        {
            get { return _chargeVoltage; }
            set
            {
                if (value < 0) throw new ArgumentException(string.Format(Resources.Errors.LessThanX, 0));
                if (value > MaxChargeVoltage) throw new ArgumentException(string.Format(Resources.Errors.GreaterThanX, MaxChargeVoltage));
                OnPropertyCanChange(value);
                if (!SetProperty(ref _chargeVoltage, value)) return;
                if (PreCutoutVoltage > ChargeVoltage) PreCutoutVoltage = 0;
                UpdateMaxValues();
            }
        }

        public double ChargeCurrent
        {
            get { return _chargeCurrent; }
            set
            {
                if (value < 0) throw new ArgumentException(string.Format(Resources.Errors.LessThanX, 0));
                if (value > MaxCurrent) throw new ArgumentException(string.Format(Resources.Errors.GreaterThanX, MaxCurrent));
                if (!SetProperty(ref _chargeCurrent, value)) return;
                if (PreCurrent > ChargeCurrent) PreCurrent = 0;
                if (ChargeCutoutCurrent > ChargeCurrent) ChargeCutoutCurrent = 0;
                UpdatePercentages();
                UpdateMaxValues();
            }
        }

        public double PreCutoutVoltage
        {
            get { return _preCutoutVoltage; }
            set
            {
                if (value < 0) throw new ArgumentException(string.Format(Resources.Errors.LessThanX, 0));
                if ((value > 0) && (value >= ChargeVoltage)) throw new ArgumentException(string.Format(Resources.Errors.GreaterEqualThanX, ChargeVoltage));
                SetProperty(ref _preCutoutVoltage, value);
            }
        }

        public double PreCurrent
        {
            get { return _preCurrent; }
            set
            {
                if (value < 0) throw new ArgumentException(string.Format(Resources.Errors.LessThanX, 0));
                if ((value > 0) && (value >= ChargeCurrent)) throw new ArgumentException(string.Format(Resources.Errors.GreaterEqualThanX, ChargeCurrent));
                if (!SetProperty(ref _preCurrent, value)) return;
                UpdatePercentages();
            }
        }

        public double ChargeCutoutCurrent
        {
            get { return _chargeCutoutCurrent; }
            set
            {
                if (value < 0) throw new ArgumentException(string.Format(Resources.Errors.LessThanX, 0));
                if ((value > 0) && (value >= ChargeCurrent)) throw new ArgumentException(string.Format(Resources.Errors.GreaterEqualThanX, ChargeCurrent));
                if (!SetProperty(ref _chargeCutoutCurrent, value)) return;
                UpdatePercentages();
            }
        }

        public int SelectedSetupIndex
        {
            get { return _selectedSetupIndex; }
            set
            {
                if (!SetProperty(ref _selectedSetupIndex, value)) return;
                LoadChannelSetup(SelectedSetupIndex);
            }
        }

        public bool SkipConstantU
        {
            get { return _skipConstantU; }
            set { SetProperty(ref _skipConstantU, value); }
        }

        public bool BeepWhenCharged
        {
            get { return _beepWhenCharged; }
            set { SetProperty(ref _beepWhenCharged, value); }
        }

        #region Commands

        protected void SaveSetupExecute(object obj)
        {
            LogTools.Enter();
            try
            {
                var fileName = _fileService.GetSaveSetupFileName();
                if (fileName == null) return;
                var channelSetup = new ChannelSetup
                {
                    ChargeVoltage = ChargeVoltage,
                    ChargeCurrent = ChargeCurrent,
                    ChargeCutoutCurrent = ChargeCutoutCurrent,
                    PreCutoutVoltage = PreCutoutVoltage,
                    PreCurrent = PreCurrent,
                    SkipConstantU = SkipConstantU,
                    BeepWhenCharged = BeepWhenCharged
                };
                _savedFileName = Path.GetFileNameWithoutExtension(fileName); 
                _fileService.WriteChannelSetup(fileName, channelSetup);
                LogTools.Exit();
            }
            catch (Exception exception)
            {
                _messagingService.Raise(this, new DisplayStatusMessageArgs { Message = LogTools.Error(exception) });
            }
        }

        #endregion

        #region private

        private void SetupAvailableSetups()
        {
            var oldSelectedSetup = string.IsNullOrEmpty(_savedFileName) ? (((SelectedSetupIndex < 0) || (AvailableSetups.Count < SelectedSetupIndex)) ? string.Empty : AvailableSetups[SelectedSetupIndex]) : _savedFileName;
            _savedFileName = null;
            AvailableSetups.Clear();
            foreach (var fileName in _fileService.GetChannelSetupNames()) AvailableSetups.Add(fileName);
            SelectedSetupIndex = AvailableSetups.Contains(oldSelectedSetup) ? AvailableSetups.IndexOf(oldSelectedSetup) : -1;
        }

        private void LoadChannelSetup(int selectedSetupIndex)
        {
            LogTools.Enter(selectedSetupIndex);
            try
            {
                if ((selectedSetupIndex < 0) || (AvailableSetups.Count < selectedSetupIndex)) return;
                var channelSetup = _fileService.GetChannelSetup(AvailableSetups[selectedSetupIndex]);
                ChargeVoltage = channelSetup.ChargeVoltage;
                ChargeCurrent = channelSetup.ChargeCurrent;
                PreCutoutVoltage = channelSetup.PreCutoutVoltage;
                PreCurrent = channelSetup.PreCurrent;
                ChargeCutoutCurrent = channelSetup.ChargeCutoutCurrent;
                SkipConstantU = channelSetup.SkipConstantU;
                BeepWhenCharged = channelSetup.BeepWhenCharged;
                LogTools.Exit(selectedSetupIndex);
            }
            catch (Exception exception)
            {
                _messagingService.Raise(this, new DisplayStatusMessageArgs { Message = LogTools.Error(selectedSetupIndex, exception) });
            }
        }

        private void SetupFilesChanged(object sender, FileSystemEventArgs e)
        {
            SynchronizationContext.Send(x => SetupAvailableSetups(), null);
        }

        private void UpdatePercentages()
        {
            OnPropertyChanged(nameof(PreCurrentPercent));
            OnPropertyChanged(nameof(ChargeCutoutCurrentPercent));
        }

        private void UpdateMaxValues()
        {
            OnPropertyChanged(nameof(MaxChargeVoltage));
            OnPropertyChanged(nameof(MaxChargeCutoutCurrent));
            OnPropertyChanged(nameof(MaxPreCurrent));
            OnPropertyChanged(nameof(MaxPreCutoutVoltage));
        }

        #endregion
    }
}
