﻿using Dps5315LiIoCharger.Dps5315;
using Dps5315LiIoCharger.Services;
using Dps5315LiIoCharger.Tools;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Timers;
using System.Globalization;
using System.Windows;
using Dps5315LiIoCharger.DataObjects;
using System.Reflection;
using Infralution.Localization.Wpf;
using Dps5315LiIoCharger.Events;

namespace Dps5315LiIoCharger.ViewModels
{
    public class MainWindowViewModel : WindowViewModel, IMainWindowViewModel
    {
        private readonly IDpsCommunication _dpsCommunication;
        private readonly IConfigService _configService;
        private readonly IMessagingService _messagingService;
        private readonly Timer _timer;
        private ConnectInformation _connectInformation;
        private TemperatureInformation _temperatureInformation;
        private CultureInfo _selectedCulture;
        private DpsMode _dpsMode;

        public MainWindowViewModel(IDpsCommunication dpsCommunication, IChannelViewModel masterChannel, IChannelViewModel slaveChannel, IConfigService configService, IMessagingService messagingService)
        {
            if (dpsCommunication == null) throw new ArgumentNullException(nameof(dpsCommunication));
            if (masterChannel == null) throw new ArgumentNullException(nameof(masterChannel));
            if (slaveChannel == null) throw new ArgumentNullException(nameof(slaveChannel));
            if (configService == null) throw new ArgumentNullException(nameof(configService));
            if (messagingService == null) throw new ArgumentNullException(nameof(messagingService));
            _dpsCommunication = dpsCommunication;
            _configService = configService;
            _messagingService = messagingService;

            MasterChannel = masterChannel;
            SlaveChannel = slaveChannel;
            SetupChannel(MasterChannel, 0);
            SetupChannel(SlaveChannel, 1);

            ConnectCommand = new RelayCommand(ConnectExecute, ConnectCanExecute);

            AvailableCultures = new List<CultureInfo> { new CultureInfo("en"), new CultureInfo("de"), new CultureInfo("da") };
            var configCulture = AvailableCultures.FirstOrDefault(x => string.Equals(x.Name, _configService.Configuration.SelectedCulture, StringComparison.OrdinalIgnoreCase));
            SelectedCulture = configCulture ?? AvailableCultures[0];

            _messagingService.Subscribe<DisplayStatusMessageArgs>(DisplayStatusMessageHandler);

            _timer = new Timer(1000);
            _timer.Elapsed += TimerElapsed;

            DpsMode = DpsMode.MasterSlave;
            Title = string.Format(Resources.MainWindow.Title, VersionNumber);
        }

        private void DisplayStatusMessageHandler(object sender, DisplayStatusMessageArgs e)
        {
            if (e == null) return;
            UpdateAppStatus(e.Message);
        }

        private DpsMode DpsMode
        {
            get { return _dpsMode; }
            set
            {
                _dpsMode = value;
                OnPropertyChanged(nameof(DpsModeDisplayText));
            }
        }

        private Version VersionNumber => Assembly.GetExecutingAssembly().GetName().Version;

        public IChannelViewModel MasterChannel { get; }
        public IChannelViewModel SlaveChannel { get; }
        public IRelayCommand ConnectCommand { get; }
        public IList<CultureInfo> AvailableCultures { get; }

        public bool IsConnected => _dpsCommunication.IsConnected;
        public string ConnectButtonDisplayText => IsConnected ? Resources.MainWindow.ConnectButtonDisconnect : Resources.MainWindow.ConnectButtonConnect;
        public string ConnectStatusDisplayText => IsConnected ? $"{_connectInformation.Port}: {_connectInformation.BaudRate}" : Resources.MainWindow.ConnectStatusNotConnected;
        public string TemperatureDisplayText => _temperatureInformation == null ? string.Format(Resources.MainWindow.TemperatureDisplayText, "-", "-") : string.Format(Resources.MainWindow.TemperatureDisplayText, _temperatureInformation.BoardTemperature, _temperatureInformation.TrafoTemperature);
        public string DpsModeDisplayText => string.Format(Resources.MainWindow.DpsModeDisplayText, DpsMode);

        public void Closing()
        {
            Disconnect();
        }

        public CultureInfo SelectedCulture
        {
            get { return _selectedCulture; }
            set
            {
                if (!SetProperty(ref _selectedCulture, value)) return;
                CultureManager.UICulture = SelectedCulture;
                _configService.Configuration.SelectedCulture = SelectedCulture.Name;
            }
        }

        #region Commands

        protected void ConnectExecute(object obj)
        {
            LogTools.Enter();
            try
            {
                if (_dpsCommunication.IsConnected) Disconnect();
                else Connect();
                LogTools.Exit();
            }
            catch (Exception exception)
            {
                LogTools.Error(exception);
                UpdateAppStatus(string.Format(_dpsCommunication.IsConnected ? Resources.Errors.ErrorDisconnecting : Resources.Errors.ErrorConnecting, exception.Message)); 
            }
        }

        protected bool ConnectCanExecute(object obj)
        {
            return true;
        }

        #endregion

        #region Events

        private void ChannelStateChanging(object sender, ChannelStateChangingArgs e)
        {
            var logArgs = LogTools.Enter(new { IsMaster = sender == MasterChannel, Args = e });
            try
            {
                var channel = sender as IChannelViewModel;
                if (channel == null) throw new ArgumentException(Resources.Errors.SenderIsNotAChannel);
                var otherChannel = Otherchannel(channel);
                if (otherChannel.ChannelState != ChannelState.Stopped)
                {
                    if (otherChannel.ChannelInput.ChargeVoltage > ChannelInputViewModel.MaxMasterSlaveVoltage) throw new InvalidOperationException(Resources.Errors.PsuAlreadyInSeries);
                    if (channel.ChannelInput.ChargeVoltage > ChannelInputViewModel.MaxMasterSlaveVoltage) throw new InvalidOperationException(Resources.Errors.OtherChannelAlreadyRunning);
                }

                var masterState = IsMaster(channel) ? e.State : MasterChannel.ChannelState;
                var slaveState = IsMaster(channel) ? SlaveChannel.ChannelState : e.State;
                var dpsMode = GetDpsMode(masterState, slaveState);

                if (dpsMode == DpsMode.Series)
                {
                    LogTools.Status(new { dpsMode }, logArgs);
                    _dpsCommunication.WriteRead<SetModeResponse>(new SetModeRequest(DpsMode.SeriesInActive));
                    _dpsCommunication.WriteRead<ClearResponse>(new ClearRequest());
                    _dpsCommunication.WriteRead<SeriesResponse>(new SeriesRequest());
                    _dpsCommunication.WriteRead<SetLimitsResponse>(new SetSeriesLimitsRequest(0, 0));
                    _dpsCommunication.WriteRead<SetModeResponse>(new SetModeRequest(DpsMode.Series));
                }
                else
                {
                    var masterVoltage = IsMaster(channel) ? 0 : MasterChannel.ChargingVoltage;
                    var masterCurrent = IsMaster(channel) ? 0 : MasterChannel.ChargingCurrent;
                    var slaveVoltage = IsMaster(channel) ? SlaveChannel.ChargingVoltage : 0;
                    var slavecurrent = IsMaster(channel) ? SlaveChannel.ChargingCurrent : 0;
                    LogTools.Status(new { dpsMode, masterVoltage, masterCurrent, slaveVoltage, slavecurrent }, logArgs);
                    _dpsCommunication.WriteRead<SetModeResponse>(new SetModeRequest(dpsMode));
                    _dpsCommunication.WriteRead<SetLimitsResponse>(new SetMasterSlaveLimitsRequest(masterVoltage, masterCurrent, slaveVoltage, slavecurrent));
                }

                DpsMode = dpsMode;

                LogTools.Exit(logArgs);
            }
            catch (Exception exception)
            {
                e.CanChange = false;
                LogTools.Error(logArgs, exception);
                UpdateAppStatus(string.Format(Resources.Errors.ErrorStateChange, exception.Message)); 
            }
        }

        private void ChannelLimitsChanging(object sender, ChannelLimitsChangingArgs e)
        {
            var logArgs = LogTools.Enter(new { IsMaster = sender == MasterChannel, Args = e });
            try
            {
                var channel = sender as IChannelViewModel;
                if (channel == null) throw new ArgumentException(Resources.Errors.SenderIsNotAChannel);
                if (channel.ChannelState == ChannelState.Stopped) return;

                var dpsMode = GetDpsMode(MasterChannel.ChannelState, SlaveChannel.ChannelState);
                if (dpsMode == DpsMode.Series)
                {
                    LogTools.Status(new { dpsMode, e.Voltage, e.Current }, logArgs);
                    _dpsCommunication.WriteRead<SetLimitsResponse>(new SetSeriesLimitsRequest(e.Voltage, e.Current));
                }
                else
                {
                    var masterVoltage = IsMaster(channel) ? e.Voltage : MasterChannel.ChargingVoltage;
                    var masterCurrent = IsMaster(channel) ? e.Current : MasterChannel.ChargingCurrent;
                    var slaveVoltage = IsMaster(channel) ? SlaveChannel.ChargingVoltage : e.Voltage;
                    var slavecurrent = IsMaster(channel) ? SlaveChannel.ChargingCurrent : e.Current;
                    LogTools.Status(new { dpsMode, masterVoltage, masterCurrent, slaveVoltage, slavecurrent }, logArgs);
                    _dpsCommunication.WriteRead<SetLimitsResponse>(new SetMasterSlaveLimitsRequest(masterVoltage, masterCurrent, slaveVoltage, slavecurrent));
                }

                LogTools.Exit(logArgs);
            }
            catch (Exception exception)
            {
                e.CanChange = false;
                LogTools.Error(logArgs, exception);
                UpdateAppStatus(string.Format(Resources.Errors.ErrorLimitChange, exception.Message)); 
            }
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (!_dpsCommunication.IsConnected) return;
            Application.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    var infoResponse = _dpsCommunication.WriteRead<InfoResponse>(new InfoRequest());
                    LogTools.Status(new { infoResponse.Mode, MasterU = infoResponse.ActualUI.Master.Voltage, MasterI = infoResponse.ActualUI.Master.Current, SlaveU = infoResponse.ActualUI.Slave.Voltage, SlaveI = infoResponse.ActualUI.Slave.Current });
                    if (infoResponse.Mode == DpsMode.Series)
                    {
                        var voltage = infoResponse.ActualUI.Master.Voltage + infoResponse.ActualUI.Slave.Voltage;
                        var current = (infoResponse.ActualUI.Master.Current + infoResponse.ActualUI.Slave.Current) / 2.0;
                        HandleChannelProgress(MasterChannel.ChannelState == ChannelState.Started ? MasterChannel : SlaveChannel, true, voltage, current);
                    }
                    else
                    {
                        HandleChannelProgress(MasterChannel, infoResponse.Mode == DpsMode.Master || infoResponse.Mode == DpsMode.MasterSlave, infoResponse.ActualUI.Master.Voltage, infoResponse.ActualUI.Master.Current);
                        HandleChannelProgress(SlaveChannel, infoResponse.Mode == DpsMode.Slave || infoResponse.Mode == DpsMode.MasterSlave, infoResponse.ActualUI.Slave.Voltage, infoResponse.ActualUI.Slave.Current);
                    }

                    _temperatureInformation = new TemperatureInformation(infoResponse.TemperatureInformation);
                    OnPropertyChanged(nameof(TemperatureDisplayText));
                }
                catch (Exception exception)
                {
                    UpdateAppStatus(LogTools.Error(exception));
                }
            });
        }

        #endregion

        #region private

        private bool IsMaster(object obj)
        {
            return ((obj is IChannelViewModel) && (obj == MasterChannel));
        }

        private IChannelViewModel Otherchannel(IChannelViewModel channel)
        {
            return IsMaster(channel) ? SlaveChannel : MasterChannel; 
        }

        private DpsMode GetDpsMode(ChannelState masterState, ChannelState slaveState)
        {
            var masterStarted = masterState == ChannelState.Started;
            var slaveStarted = slaveState == ChannelState.Started;

            var isSeries = (masterStarted && MasterChannel.ChannelInput.ChargeVoltage > ChannelInputViewModel.MaxMasterSlaveVoltage) || (slaveStarted && SlaveChannel.ChannelInput.ChargeVoltage > ChannelInputViewModel.MaxMasterSlaveVoltage);
            return isSeries ? DpsMode.Series : GetMasterSlaveDpsMode(masterStarted, slaveStarted);
        }

        private DpsMode GetMasterSlaveDpsMode(bool masterStarted, bool slaveStarted)
        {
            if (masterStarted && slaveStarted) return DpsMode.MasterSlave;
            else if (masterStarted) return DpsMode.Master;
            else if (slaveStarted) return DpsMode.Slave;
            else return DpsMode.MasterSlaveInActive;
        }

        private void UpdateAppStatus(string status)
        {
            MessageBox.Show(status, Title, MessageBoxButton.OK);
        }

        private void UpdateConnectProperties()
        {
            OnPropertyChanged(nameof(IsConnected));
            OnPropertyChanged(nameof(ConnectButtonDisplayText));
            OnPropertyChanged(nameof(ConnectStatusDisplayText));
        }

        private void SetupChannel(IChannelViewModel channel, int channelNumber)
        {
            channel.ChannelStateChanging += ChannelStateChanging;
            channel.ChannelLimitsChanging += ChannelLimitsChanging;
            channel.IsChannelConnected = false;
            channel.Init(channelNumber);
        }

        private void HandleChannelProgress(IChannelViewModel channel, bool isChannelActive, double voltage, double current)
        {
            if (!isChannelActive) return;
            channel.ChargeProgress(voltage, current);
        }

        private void Connect()
        {
            if (_dpsCommunication.IsConnected) return;

            _connectInformation = _dpsCommunication.Connect();
            _dpsCommunication.WriteRead<ConnectResponse>(new ConnectRequest());
            _dpsCommunication.WriteRead<SetModeResponse>(new SetModeRequest(DpsMode.MasterSlaveInActive));
            MasterChannel.IsChannelConnected = true;
            SlaveChannel.IsChannelConnected = true;
            _timer.Start();
            UpdateConnectProperties();
        }

        private void Disconnect()
        {
            if (!_dpsCommunication.IsConnected) return;

            _timer.Stop();
            MasterChannel.IsChannelConnected = false;
            SlaveChannel.IsChannelConnected = false;
            _dpsCommunication.WriteRead<SetModeResponse>(new SetModeRequest(DpsMode.MasterSlaveInActive, false));
            _dpsCommunication.Disconnect();
            _connectInformation = null;
            _temperatureInformation = null;
            OnPropertyChanged(nameof(TemperatureDisplayText));
            UpdateConnectProperties();
        }

        #endregion
    }
}
