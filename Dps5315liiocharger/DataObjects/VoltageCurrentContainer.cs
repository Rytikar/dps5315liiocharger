﻿namespace Dps5315LiIoCharger.DataObjects
{
    public struct VoltageCurrentContainer
    {
        public double Voltage;
        public double Current;

        public VoltageCurrentContainer(double voltage, double current)
        {
            Voltage = voltage;
            Current = current;
        }
    }
}
