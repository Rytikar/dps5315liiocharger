﻿namespace Dps5315LiIoCharger.DataObjects
{
    public class ChannelSetup
    {
        public double PreCutoutVoltage { get; set; }
        public double PreCurrent { get; set; }
        public double ChargeCurrent { get; set; }
        public double ChargeVoltage { get; set; }
        public double ChargeCutoutCurrent { get; set; }
        public bool SkipConstantU { get; set; }
        public bool BeepWhenCharged { get; set; }
    }
}
