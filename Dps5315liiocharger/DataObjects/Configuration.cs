﻿using Dps5315LiIoCharger.Tools;
using OxyPlot;
using System.Collections.Generic;
using System.Globalization;

namespace Dps5315LiIoCharger.DataObjects
{
    public class Configuration : NotifyableBase
    {
        private string _selectedCulture;

        public Configuration()
        {
            SelectedCulture = CultureInfo.CurrentCulture.Name.Split('-')[0];
        }

        public string SelectedCulture
        {
            get { return _selectedCulture; }
            set { SetProperty(ref _selectedCulture, value); }
        }

        public IList<string> LastSetups{ get; set; } = new List<string>(2);
        public IList<LegendPosition> LegendPositions { get; set; } = new List<LegendPosition>(2);

        public void SetLastSetup(int channelNumber, string setup)
        {
            LastSetups[channelNumber] = setup;
            OnPropertyChanged(nameof(LastSetups));
        }

        public void SetLegendPosition(int channelNumber, LegendPosition legendPosition)
        {
            LegendPositions[channelNumber] = legendPosition;
            OnPropertyChanged(nameof(LegendPositions));
        }
    }
}
