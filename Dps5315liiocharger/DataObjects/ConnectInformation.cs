﻿namespace Dps5315LiIoCharger.DataObjects
{
    public class ConnectInformation
    {
        public string Port { get; }
        public int BaudRate { get; }

        public ConnectInformation(string port, int baudRate)
        {
            Port = port;
            BaudRate = baudRate;
        }
    }
}
