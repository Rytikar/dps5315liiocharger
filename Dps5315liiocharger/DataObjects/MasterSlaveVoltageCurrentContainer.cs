﻿namespace Dps5315LiIoCharger.DataObjects
{
    public struct MasterSlaveVoltageCurrentContainer
    {
        public VoltageCurrentContainer Master;
        public VoltageCurrentContainer Slave;

        public MasterSlaveVoltageCurrentContainer(double masterVoltage, double masterCurrent, double slaveVoltage, double slaveCurrent)
        {
            Master.Voltage = masterVoltage;
            Master.Current = masterCurrent;
            Slave.Voltage = slaveVoltage;
            Slave.Current = slaveCurrent;
        }
    }
}
