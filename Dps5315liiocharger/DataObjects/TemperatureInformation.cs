﻿namespace Dps5315LiIoCharger.DataObjects
{
    public class TemperatureInformation
    {
        public int BoardTemperature { get; }
        public int TrafoTemperature { get; }
        public bool BoardOverheated { get; }
        public bool TrafoOverheated { get; }

        public TemperatureInformation(int boardTemperature, int trafoTemperature, bool boardOverheated, bool trafoOverheated)
        {
            BoardTemperature = boardTemperature;
            TrafoTemperature = trafoTemperature;
            BoardOverheated = boardOverheated;
            TrafoOverheated = trafoOverheated;
        }

        public TemperatureInformation(TemperatureInformation temperatureInformation)
        {
            BoardTemperature = temperatureInformation.BoardTemperature;
            TrafoTemperature = temperatureInformation.TrafoTemperature;
            BoardOverheated = temperatureInformation.BoardOverheated;
            TrafoOverheated = temperatureInformation.TrafoOverheated;
        }
    }
}
