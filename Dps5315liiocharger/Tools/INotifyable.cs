﻿using System.ComponentModel;

namespace Dps5315LiIoCharger.Tools
{
    public interface INotifyable : INotifyPropertyChanged
    {
    }
}
