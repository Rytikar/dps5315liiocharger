﻿using System;
using System.Runtime.CompilerServices;
using log4net;
using System.IO;

namespace Dps5315LiIoCharger.Tools
{
    public static class LogTools
    {
        public static void Enter([CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "")
        {
            GetLogger(callerFilePath).Debug($"{callerMemberName} entered.");
        }

        public static object Enter(object args, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "")
        {
            GetLogger(callerFilePath).Debug($"{callerMemberName}({args}) entered.");
            return args;
        }

        public static void Exit([CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "")
        {
            GetLogger(callerFilePath).Debug($"{callerMemberName} exited.");
        }

        public static object Exit(object args, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "")
        {
            GetLogger(callerFilePath).Debug($"{callerMemberName}({args}) exited.");
            return args;
        }

        public static object Exit(object result, object args, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "")
        {
            GetLogger(callerFilePath).Debug($"{callerMemberName}({args}) exited. Result: {result}.");
            return args;
        }

        public static string Error(object args, Exception exitException, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "")
        {
            var msg = $"{callerMemberName}({args}) exited with error.";
            GetLogger(callerFilePath).Error(msg, exitException);
            return msg;
        }

        public static string Error(Exception exitException, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "")
        {
            var msg = $"{callerMemberName} exited with error.";
            GetLogger(callerFilePath).Error(msg, exitException);
            return msg;
        }

        public static string Error(string msg, Exception exitException, [CallerFilePath] string callerFilePath = "")
        {
            GetLogger(callerFilePath).Error(msg, exitException);
            return msg;
        }

        public static string Error(string msg, [CallerFilePath] string callerFilePath = "")
        {
            GetLogger(callerFilePath).Error(msg);
            return msg;
        }

        public static void Status(object status, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "")
        {
            GetLogger(callerFilePath).Info($"{callerMemberName} status: {status}");
        }

        public static object Status(object status, object args, [CallerMemberName] string callerMemberName = "", [CallerFilePath] string callerFilePath = "")
        {
            GetLogger(callerFilePath).Info($"{callerMemberName}({args}) status: {status}");
            return args;
        }

        private static ILog GetLogger(string callerFileName)
        {
            return LogManager.GetLogger(Path.GetFileNameWithoutExtension(callerFileName));
        }
    }
}
