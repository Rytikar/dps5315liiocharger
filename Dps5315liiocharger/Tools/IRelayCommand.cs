﻿using System.Windows.Input;

namespace Dps5315LiIoCharger.Tools
{
    public interface IRelayCommand : ICommand
    {
        void RaiseCanExecuteChanged();
    }
}
