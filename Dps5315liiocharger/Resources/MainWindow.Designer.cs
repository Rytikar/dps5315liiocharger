﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dps5315LiIoCharger.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class MainWindow {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal MainWindow() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Dps5315LiIoCharger.Resources.MainWindow", typeof(MainWindow).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connect.
        /// </summary>
        internal static string ConnectButtonConnect {
            get {
                return ResourceManager.GetString("ConnectButtonConnect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Disconnect.
        /// </summary>
        internal static string ConnectButtonDisconnect {
            get {
                return ResourceManager.GetString("ConnectButtonDisconnect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not Connected.
        /// </summary>
        internal static string ConnectStatusNotConnected {
            get {
                return ResourceManager.GetString("ConnectStatusNotConnected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mode: {0}.
        /// </summary>
        internal static string DpsModeDisplayText {
            get {
                return ResourceManager.GetString("DpsModeDisplayText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Master.
        /// </summary>
        internal static string GroupBoxMaster {
            get {
                return ResourceManager.GetString("GroupBoxMaster", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Slave.
        /// </summary>
        internal static string GroupBoxSlave {
            get {
                return ResourceManager.GetString("GroupBoxSlave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Board: {0}C, Trafo: {1}C.
        /// </summary>
        internal static string TemperatureDisplayText {
            get {
                return ResourceManager.GetString("TemperatureDisplayText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dps5315 LiIo Charger ({0}).
        /// </summary>
        internal static string Title {
            get {
                return ResourceManager.GetString("Title", resourceCulture);
            }
        }
    }
}
