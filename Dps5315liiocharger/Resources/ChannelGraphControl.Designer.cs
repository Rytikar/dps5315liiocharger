﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dps5315LiIoCharger.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ChannelGraphControl {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ChannelGraphControl() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Dps5315LiIoCharger.Resources.ChannelGraphControl", typeof(ChannelGraphControl).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Amp.
        /// </summary>
        internal static string AmpAxisTitle {
            get {
                return ResourceManager.GetString("AmpAxisTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I.
        /// </summary>
        internal static string AmpSeriesTitle {
            get {
                return ResourceManager.GetString("AmpSeriesTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear.
        /// </summary>
        internal static string ClearButtonContent {
            get {
                return ResourceManager.GetString("ClearButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Legend.
        /// </summary>
        internal static string LegendLabelContent {
            get {
                return ResourceManager.GetString("LegendLabelContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bottom Left.
        /// </summary>
        internal static string LegendPositionBottomLeft {
            get {
                return ResourceManager.GetString("LegendPositionBottomLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bottom Right.
        /// </summary>
        internal static string LegendPositionBottomRight {
            get {
                return ResourceManager.GetString("LegendPositionBottomRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Top Left.
        /// </summary>
        internal static string LegendPositionTopLeft {
            get {
                return ResourceManager.GetString("LegendPositionTopLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Top Right.
        /// </summary>
        internal static string LegendPositionTopRight {
            get {
                return ResourceManager.GetString("LegendPositionTopRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Load.
        /// </summary>
        internal static string LoadButtonContent {
            get {
                return ResourceManager.GetString("LoadButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Load channel graph data.
        /// </summary>
        internal static string LoadGraphTitle {
            get {
                return ResourceManager.GetString("LoadGraphTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        internal static string SaveButtonContent {
            get {
                return ResourceManager.GetString("SaveButtonContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save channel graph data.
        /// </summary>
        internal static string SaveGraphTitle {
            get {
                return ResourceManager.GetString("SaveGraphTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time.
        /// </summary>
        internal static string TimeAxisTitle {
            get {
                return ResourceManager.GetString("TimeAxisTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Volt.
        /// </summary>
        internal static string VoltAxisTitle {
            get {
                return ResourceManager.GetString("VoltAxisTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to U.
        /// </summary>
        internal static string VoltSeriesTitle {
            get {
                return ResourceManager.GetString("VoltSeriesTitle", resourceCulture);
            }
        }
    }
}
