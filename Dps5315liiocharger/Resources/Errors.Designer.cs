﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Dps5315LiIoCharger.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Errors {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Errors() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Dps5315LiIoCharger.Resources.Errors", typeof(Errors).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot change limits!.
        /// </summary>
        internal static string ErrorChargeVoltageChanging {
            get {
                return ResourceManager.GetString("ErrorChargeVoltageChanging", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error disconnecting\n\n{0}.
        /// </summary>
        internal static string ErrorConnecting {
            get {
                return ResourceManager.GetString("ErrorConnecting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error connecting!\n\n{0}.
        /// </summary>
        internal static string ErrorDisconnecting {
            get {
                return ResourceManager.GetString("ErrorDisconnecting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error changing channel limits!\n\n{0}.
        /// </summary>
        internal static string ErrorLimitChange {
            get {
                return ResourceManager.GetString("ErrorLimitChange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error changing channel state!\n\n{0}.
        /// </summary>
        internal static string ErrorStateChange {
            get {
                return ResourceManager.GetString("ErrorStateChange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Must be less than {0}!.
        /// </summary>
        internal static string GreaterEqualThanX {
            get {
                return ResourceManager.GetString("GreaterEqualThanX", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Must be less or equal than {0}!.
        /// </summary>
        internal static string GreaterThanX {
            get {
                return ResourceManager.GetString("GreaterThanX", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Must be greater than {0}!.
        /// </summary>
        internal static string LessThanX {
            get {
                return ResourceManager.GetString("LessThanX", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other channel already running. Charge voltage must be below or equal 15!.
        /// </summary>
        internal static string OtherChannelAlreadyRunning {
            get {
                return ResourceManager.GetString("OtherChannelAlreadyRunning", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Psu already running in series!.
        /// </summary>
        internal static string PsuAlreadyInSeries {
            get {
                return ResourceManager.GetString("PsuAlreadyInSeries", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sender is not a Channel!.
        /// </summary>
        internal static string SenderIsNotAChannel {
            get {
                return ResourceManager.GetString("SenderIsNotAChannel", resourceCulture);
            }
        }
    }
}
