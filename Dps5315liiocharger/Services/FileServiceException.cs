﻿using System;

namespace Dps5315LiIoCharger.Services
{
    public class FileServiceException : Exception
    {
        public FileServiceException(string msg, Exception innerException = null) : base(msg, innerException)
        { }
    }
}
