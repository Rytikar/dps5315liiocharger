﻿using Dps5315LiIoCharger.DataObjects;

namespace Dps5315LiIoCharger.Services
{
    public interface IConfigService
    {
        Configuration Configuration { get; }
    }
}
