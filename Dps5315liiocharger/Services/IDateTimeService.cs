﻿using System;

namespace Dps5315LiIoCharger.Services
{
    public interface IDateTimeService
    {
        DateTime Now { get; }
    }
}
