﻿using Dps5315LiIoCharger.Dps5315;

namespace Dps5315LiIoCharger.Services
{
    public interface IDpsResponseFactory
    {
        T Get<T>() where T : IDpsResponse;
    }
}
