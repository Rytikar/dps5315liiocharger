﻿using System;

namespace Dps5315LiIoCharger.Services
{
    public class SerialServiceException : Exception
    {
        public SerialServiceException(string msg, Exception innerException = null) : base(msg, innerException)
        { }
    }
}
