﻿using System;

namespace Dps5315LiIoCharger.Services
{
    public class DateTimeService : IDateTimeService
    {
        public DateTime Now => DateTime.Now;
    }
}
