﻿using System.Collections.Generic;

namespace Dps5315LiIoCharger.Services
{
    public interface ISerialService
    {
        void Open(string port, int baudRate);
        void Close();
        IList<SerialInformation> GetSerialInformation();
        void WriteFrame(byte[] data);
        byte[] ReadFrame();
        bool IsOpen { get; }
        string Port { get; }
        int BaudRate { get; }
    }
}
