﻿using System;
using System.Collections.Generic;

namespace Dps5315LiIoCharger.Services
{
    public class CrcService : ICrcService
    {
        public CrcService()
        {
            Reset();
        }

        public ushort CheckSum { get; private set; }

        public void Calc(byte[] values)
        {
            foreach (var b in values) Calc(b);
        }

        public void Calc(byte value)
        {
            for (byte index = 0; index < 8; ++index)
            {
                var flag = (CheckSum & 32768) != 0;
                CheckSum <<= 1;
                if ((value & 128) != 0) CheckSum |= 1;
                value <<= 1;
                if (flag) CheckSum ^= 32773;
            }
        }

        public void Reset()
        {
            CheckSum = ushort.MaxValue;
        }

        public ushort GetCrc(byte[] data)
        {
            Reset();
            Calc(data);
            Calc(0);
            Calc(0);
            return CheckSum;
        }

        public byte[] CheckCrc(List<byte> list)
        {
            if (list.Count < 3) return list.ToArray();

            var checkSum = (ushort)((list[list.Count - 2] << 8) + list[list.Count - 1]);
            list.RemoveRange(list.Count - 2, 2);

            var data = list.ToArray();
            if (GetCrc(data) != checkSum) throw new InvalidOperationException("Invalid Crc!");

            return data;
        }

    }
}

/*


        public const ushort POLYNOM = 32773;
        public const ushort INITAL_VALUE = 65535;
        private ushort checksum;

        public ushort Checksum
        {
            get
            {
                return this.checksum;
            }
        }

        public CRC16()
        {
            this.Init();
        }

        public CRC16 Init()
        {
            this.checksum = ushort.MaxValue;
            return this;
        }

        public CRC16 Calc(byte value)
        {
            for (byte index = 0; (int)index < 8; ++index)
            {
                bool flag = ((int)this.checksum & 32768) != 0;
                this.checksum <<= 1;
                if (((int)value & 128) != 0)
                    this.checksum |= (ushort)1;
                value <<= 1;
                if (flag)
                    this.checksum ^= (ushort)32773;
            }
            return this;
        }

        public CRC16 Calc(byte[] values)
        {
            for (int index = 0; index < values.Length; ++index)
                this.Calc(values[index]);
            return this;
        }
 
     
*/
