﻿using System.Collections.Generic;
using Dps5315LiIoCharger.DataObjects;
using System;
using System.IO;

namespace Dps5315LiIoCharger.Services
{
    public interface IFileService
    {
        IEnumerable<string> GetChannelSetupNames();
        ChannelSetup GetChannelSetup(string fileName);
        string GetSaveSetupFileName();
        string GetSaveGraphFileName();
        string GetLoadGraphFileName();
        Configuration GetConfiguration();
        void WriteConfiguration(Configuration configuration);
        void WriteChannelSetup(string fileName, ChannelSetup channelSetup);
        void WriteTextFile(string fileName, Func<int, string> getLineFunc);
        void ReadTextFile(string fileName, Action<string> readLineAction);
        event EventHandler<FileSystemEventArgs> SetupFilesChanged;
    }
}
