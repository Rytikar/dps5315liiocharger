﻿using System;
using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Dps5315;

namespace Dps5315LiIoCharger.Services
{
    public class DpsCommunicationEmulator : IDpsCommunication
    {
        private bool _isConnected;
        private double _masterLimitVoltage;
        private double _masterLimitCurrent;
        private double _slaveLimitVoltage;
        private double _slaveLimitCurrent;
        private double _masterActualVoltage;
        private double _masterActualCurrent;
        private double _slaveActualVoltage;
        private double _slaveActualCurrent;
        private DpsMode _mode = DpsMode.MasterSlaveInActive;

        public int BaudRate => 115200;
        public bool IsConnected => _isConnected;
        public string Port => "COM3";

        public ConnectInformation Connect()
        {
            _isConnected = true;
            return new ConnectInformation(Port, BaudRate);
        }

        public void Disconnect()
        {
            _isConnected = false;
        }

        public T WriteRead<T>(IDpsRequest request) where T : class, IDpsResponse, new()
        {
            if (request is ConnectRequest) return new T(); 
            if (request is ConnectRequest) return new T();
            if (request is SetModeRequest)
            {
                var mode = ((SetModeRequest)request).Mode;
                if (((mode == DpsMode.Master) || (mode == DpsMode.MasterSlave)) && ((_mode == DpsMode.MasterSlaveInActive) || (_mode == DpsMode.Slave)))
                {
                    _masterActualVoltage = 0;
                    _masterActualCurrent = 0;
                }
                if (((mode == DpsMode.Slave) || (mode == DpsMode.MasterSlave)) && ((_mode == DpsMode.MasterSlaveInActive) || (_mode == DpsMode.Master)))
                {
                    _slaveActualVoltage = 0;
                    _slaveActualCurrent = 0;
                }

                _mode = mode;

                return new T();
            }
            if (request is SetLimitsRequest)
            {
                _masterLimitVoltage = ((SetLimitsRequest)request).LimitsUI.Master.Voltage;
                _masterLimitCurrent = ((SetLimitsRequest)request).LimitsUI.Master.Current;
                _slaveLimitVoltage = ((SetLimitsRequest)request).LimitsUI.Slave.Voltage;
                _slaveLimitCurrent = ((SetLimitsRequest)request).LimitsUI.Slave.Current;
                return new T();
            }
            if (request is InfoRequest)
            {
                if ((_mode == DpsMode.Master) || (_mode == DpsMode.MasterSlave)) LiIoProgress(ref _masterActualVoltage, ref _masterActualCurrent, _masterLimitVoltage, _masterLimitCurrent);
                if ((_mode == DpsMode.Slave) || (_mode == DpsMode.MasterSlave)) LiIoProgress(ref _slaveActualVoltage, ref _slaveActualCurrent, _slaveLimitVoltage, _slaveLimitCurrent);

                var ir = new InfoResponse { Mode = _mode, ActualUI = new MasterSlaveVoltageCurrentContainer(_masterActualVoltage, _masterActualCurrent, _slaveActualVoltage, _slaveActualCurrent), TemperatureInformation = GetTemperatureInformation() };
                return ir as T;
            }
            throw new NotImplementedException($"Request not implemented: {request.GetType()}");
        }

        private void LiIoProgress(ref double _actualVoltage, ref double _actualCurrent, double _limitVoltage, double _limitCurrent)
        {
            if (_actualVoltage < _limitVoltage)
            {
                _actualVoltage += (_actualVoltage < 2.5) ? 0.2 : 0.1;
                if (_actualVoltage > _limitVoltage) _actualVoltage = _limitVoltage;
                _actualCurrent = _limitCurrent;
            }
            else
            {
                _actualVoltage = _limitVoltage;
                _actualCurrent -= 0.1;
                if (_actualCurrent < 0) _actualCurrent = 0;
            }
        }

        private TemperatureInformation GetTemperatureInformation()
        {
            var p = (_masterActualVoltage * _masterActualCurrent) + (_slaveActualVoltage * _slaveActualCurrent);
            var bt = Convert.ToInt32((p * 10) + 30);
            var tt = Convert.ToInt32((p * 7) + 30);
            return new TemperatureInformation(bt, tt, false, false);
        }
    }
}
