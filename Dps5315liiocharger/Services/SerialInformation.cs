﻿namespace Dps5315LiIoCharger.Services
{
    public class SerialInformation
    {
        public string Port { get; private set; }
        public string Description { get; private set; }

        public SerialInformation(string port, string description)
        {
            Port = port;
            Description = description;
        }
    }
}
