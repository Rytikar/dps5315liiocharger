﻿using System;
using System.Collections.Generic;

namespace Dps5315LiIoCharger.Services
{
    public class MessagingService : IMessagingService
    {
        private readonly IDictionary<Type, object> _map = new Dictionary<Type, object>();

        public void Raise<T>(object sender, T args) where T : EventArgs
        {
            var type = typeof(T);
            if (!_map.ContainsKey(type)) return;
            var handler = _map[type] as EventHandler<T>;
            handler?.Invoke(sender, args);
        }

        public void Subscribe<T>(EventHandler<T> subscriber) where T : EventArgs
        {
            var type = typeof(T);
            if (!_map.ContainsKey(type)) _map.Add(type, subscriber);
            else
            {
                var handler = _map[type] as EventHandler<T>;
                if (handler == null) return;
                handler += subscriber;
            }
        }

        public void UnSubscribe<T>(EventHandler<T> unsubscriber) where T : EventArgs
        {
            var type = typeof(T);
            if (!_map.ContainsKey(type)) return;
            var handler = _map[type] as EventHandler<T>;
            if (handler == null) return;
            handler -= unsubscriber;
        }
    }
}
