﻿using Dps5315LiIoCharger.ViewModels;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;

namespace Dps5315LiIoCharger.Services
{
    public interface IViewFactory
    {
        void Register<TViewModel, TView>() where TViewModel : class, IViewModel where TView : Window;
        Window Resolve<TViewModel>(Action<TViewModel> setStateAction = null) where TViewModel : class, IViewModel;
    }

    public class ViewFactory : IViewFactory
    {
        private readonly IUnityContainer _container;
        private readonly IDictionary<Type, Type> _map = new Dictionary<Type, Type>();

        public ViewFactory(IUnityContainer container)
        {
            if (container == null) throw new ArgumentNullException(nameof(container));
            _container = container;
        }

        public void Register<TViewModel, TView>() where TViewModel : class, IViewModel where TView : Window
        {
            _map.Add(typeof(TViewModel), typeof(TView));
        }

        public Window Resolve<TViewModel>(Action<TViewModel> setStateAction) where TViewModel : class, IViewModel
        {
            var viewModel = _container.Resolve<TViewModel>();
            var view = _container.Resolve(_map[typeof(TViewModel)]) as Window;
            if (view == null) throw new InvalidOperationException($"Could not resolve view for viewmodel type {typeof(TViewModel)}");
            viewModel.SynchronizationContext = SynchronizationContext.Current;
            view.DataContext = viewModel;
            return view;
        }
    }
}
