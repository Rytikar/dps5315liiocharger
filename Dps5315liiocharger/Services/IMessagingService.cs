﻿using System;

namespace Dps5315LiIoCharger.Services
{
    public interface IMessagingService
    {
        void Subscribe<T>(EventHandler<T> handler) where T : EventArgs;
        void UnSubscribe<T>(EventHandler<T> handler) where T : EventArgs;
        void Raise<T>(object sender, T args) where T : EventArgs;
    }
}
