﻿using System.Collections.Generic;

namespace Dps5315LiIoCharger.Services
{
    public interface ICrcService
    {
        ushort CheckSum { get; }
        void Calc(byte value);
        void Calc(byte[] values);
        void Reset();
        ushort GetCrc(byte[] data);
        byte[] CheckCrc(List<byte> list);
    }
}
