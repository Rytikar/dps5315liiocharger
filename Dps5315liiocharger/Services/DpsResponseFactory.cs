﻿using System;
using Dps5315LiIoCharger.Dps5315;
using Microsoft.Practices.Unity;

namespace Dps5315LiIoCharger.Services
{
    public class DpsResponseFactory : IDpsResponseFactory
    {
        private readonly IUnityContainer _container;

        public DpsResponseFactory(IUnityContainer container)
        {
            if (container == null) throw new ArgumentNullException(nameof(container));
            _container = container;
        }

        public T Get<T>() where T : IDpsResponse
        {
            return _container.Resolve<T>();
        }
    }
}
