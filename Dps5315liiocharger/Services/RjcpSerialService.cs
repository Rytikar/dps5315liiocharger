﻿using RJCP.IO.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using Dps5315LiIoCharger.Tools;

namespace Dps5315LiIoCharger.Services
{
    public class RjcpSerialService : BaseSerialService, ISerialService
    {
        private readonly SerialPortStream _serialPortStream;
        private readonly ICrcService _crcService;

        public bool IsOpen => _serialPortStream.IsOpen;

        public string Port => IsOpen ? _serialPortStream.PortName : string.Empty;

        public int BaudRate => IsOpen ? _serialPortStream.BaudRate : 0;

        public RjcpSerialService(SerialPortStream serialPortStream, ICrcService crcService)
        {
            if (serialPortStream == null) throw new ArgumentNullException(nameof(serialPortStream));
            if (crcService == null) throw new ArgumentNullException(nameof(crcService));
            _serialPortStream = serialPortStream;
            _crcService = crcService;
        }

        public void Open(string port, int baudRate)
        {
            LogTools.Enter(new {Port=port, BaudRate = baudRate });
            _serialPortStream.PortName = port;
            _serialPortStream.BaudRate = baudRate;
            _serialPortStream.Encoding = Encoding.UTF8;
            _serialPortStream.ReadTimeout = ReadTimeoutMs;
            _serialPortStream.WriteTimeout = WriteTimeoutMs;
            //_serialPortStream.Handshake = Handshake.Rts;

            try
            {
                _serialPortStream.Open();
                LogTools.Exit(new { Port = port, BaudRate = baudRate });
            }
            catch (Exception exception)
            {
                throw new SerialServiceException(LogTools.Error(new { Port = port, BaudRate = baudRate }, exception), exception);
            }
        }

        public void Close()
        {
            LogTools.Enter();
            if (!IsOpen)
            {
                LogTools.Status("Is not open!");
                LogTools.Exit();
                return;
            }
            try
            {
                _serialPortStream.Close();
                LogTools.Exit();
            }
            catch (Exception exception)
            {
                throw new SerialServiceException(LogTools.Error(exception), exception);
            }
        }

        public IList<SerialInformation> GetSerialInformation()
        {
            LogTools.Enter();
            var result = new List<SerialInformation>();
            try
            {
                using (var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity"))
                {
                    var portNames = SerialPortStream.GetPortNames();
                    foreach (var o in searcher.Get())
                    {
                        var queryObj = (ManagementObject) o;
                        var caption = queryObj["Caption"];
                        if (caption == null) continue;
                        result.AddRange(from portName in portNames where caption.ToString().Contains($"({portName})") select new SerialInformation(portName, caption.ToString()));
                    }
                }
                LogTools.Exit();
                return result;
            }
            catch(Exception exception)
            {
                throw new SerialServiceException(LogTools.Error(exception), exception);
            }
        }

        public void WriteFrame(byte[] data)
        {
            LogTools.Enter(new {Data=BitConverter.ToString(data)});
            try
            {
                if (!_serialPortStream.CanWrite) throw new InvalidOperationException("Port not ready to be written to!");

                _serialPortStream.WriteByte(FrameStart);

                foreach (var b in data) WriteEscaped(b, _serialPortStream);

                var checkSum = _crcService.GetCrc(data);
                WriteEscaped((byte)(checkSum >> 8), _serialPortStream);
                WriteEscaped((byte)(checkSum & 255), _serialPortStream);

                _serialPortStream.WriteByte(FrameEnd);

                _serialPortStream.Flush();
                LogTools.Exit();
            }
            catch (Exception exception)
            {
                throw new SerialServiceException(LogTools.Error(exception), exception);
            }
        }

        public byte[] ReadFrame()
        {
            LogTools.Enter();
            if (!_serialPortStream.CanRead) throw new InvalidOperationException("Port not ready to be read from!");
            var list = new List<byte>();
            try
            {
                do { } while (_serialPortStream.ReadByte() != FrameStart);
                int i;
                do
                {
                    i = _serialPortStream.ReadByte();
                    if ((i == -1) || (i == FrameEnd)) break;

                    if (i == EscapeByte) i = _serialPortStream.ReadByte() & 127;
                    list.Add((byte)i);
                } while (true);

                if (i != FrameEnd) throw new InvalidOperationException("Invalid frame!");

                var data = _crcService.CheckCrc(list); 

                LogTools.Exit(new {Data=BitConverter.ToString(data)});
                return data;
            }
            catch (Exception exception)
            {
                throw new SerialServiceException(LogTools.Error(exception), exception);
            }
        }

        private void WriteEscaped(byte b, SerialPortStream serialPortStream)
        {
            if (b == FrameStart || b == FrameEnd || b == EscapeByte)
            {
                serialPortStream.WriteByte(EscapeByte);
                serialPortStream.WriteByte((byte)(b | 128));
            }
            else serialPortStream.WriteByte(b);
        }
    }
}
