﻿using System;
using Dps5315LiIoCharger.Dps5315;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using Dps5315LiIoCharger.DataObjects;

namespace Dps5315LiIoCharger.Services
{
    public class DpsCommunication : IDpsCommunication
    {
        private readonly ISerialService _serialService;
        private readonly IDpsResponseFactory _dpsResponseFactory;

        public string Port => _serialService.Port;
        public int BaudRate => _serialService.BaudRate;
        public bool IsConnected => _serialService.IsOpen;

        public DpsCommunication(ISerialService serialService, IDpsResponseFactory dpsResponseFactory)
        {
            if (serialService == null) throw new ArgumentNullException(nameof(serialService));
            if (dpsResponseFactory == null) throw new ArgumentNullException(nameof(dpsResponseFactory));
            _serialService = serialService;
            _dpsResponseFactory = dpsResponseFactory;
        }

        public ConnectInformation Connect()
        {
            if (IsConnected) return new ConnectInformation(_serialService.Port, _serialService.BaudRate);

            var baudRate = int.Parse(ConfigurationManager.AppSettings["DpsBaudRate"]);
            var portDescriptions = ConfigurationManager.AppSettings["DpsPortDescriptions"];

            var serialInfos = GetSerialInfos(portDescriptions);
            if (!serialInfos.Any()) throw new InvalidOperationException("No port with DPS");
            if (serialInfos.Count > 1) throw new InvalidOperationException("More than one DPS");

            var serialInfo = serialInfos.First();
            _serialService.Open(serialInfo.Port, baudRate);
            return new ConnectInformation(_serialService.Port, _serialService.BaudRate);
        }

        public void Disconnect()
        {
            if (!IsConnected) return;

            _serialService.Close();
        }

        public T WriteRead<T>(IDpsRequest request) where T : class, IDpsResponse, new()
        {
            Write(request);
            var response = Read<T>();
            System.Threading.Thread.Sleep(100);
            return response;
        }

        private T Read<T>() where T : IDpsResponse
        {
            var response = _dpsResponseFactory.Get<T>();
            var data = _serialService.ReadFrame();
            response.Decorate(data);
            return response;
        }

        private void Write(IDpsRequest request)
        {
            _serialService.WriteFrame(request.ToBytes());
        }

        #region private

        private IList<SerialInformation> GetSerialInfos(string portDescriptions)
        {
            var split = portDescriptions.Split(',');
            if (split.Length == 0) throw new InvalidOperationException("No DPS port descriptions defined!");
            return _serialService.GetSerialInformation().Where(x => split.Any(y => x.Description.Contains(y))).ToList();
        }

        #endregion
    }
}
