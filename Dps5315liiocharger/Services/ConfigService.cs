﻿using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Tools;
using OxyPlot;
using System;

namespace Dps5315LiIoCharger.Services
{
    public class ConfigService : IConfigService
    {
        private readonly IFileService _fileService;
        private Configuration _configuration;

        public ConfigService(IFileService fileService)
        {
            if (fileService == null) throw new ArgumentNullException(nameof(fileService));
            _fileService = fileService;

            _configuration = _fileService.GetConfiguration();
            if (_configuration == null)
            {
                LogTools.Status("Setting up new configuration");
                _configuration = NewConfiguration();
                _fileService.WriteConfiguration(_configuration);
            }
            _configuration.PropertyChanged += ConfigurationPropertyChanged;
        }

        public Configuration Configuration => _configuration;

        private Configuration NewConfiguration()
        {
            var configuration = new Configuration();
            configuration.LastSetups.Add(string.Empty);
            configuration.LastSetups.Add(string.Empty);
            configuration.LegendPositions.Add(LegendPosition.TopRight);
            configuration.LegendPositions.Add(LegendPosition.TopRight);
            return configuration;
        }

        private void ConfigurationPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            LogTools.Enter();
            try
            {
                _fileService.WriteConfiguration(_configuration);
                LogTools.Exit();
            }
            catch (Exception exception)
            {
                LogTools.Error(exception);
            }
        }
    }
}
