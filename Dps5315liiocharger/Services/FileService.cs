﻿using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Tools;
using Newtonsoft.Json;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dps5315LiIoCharger.Services
{
    public class FileService : IFileService
    {
        private const string DefaultExtension = ".txt";
        private const string DataPathPart = "Dps5315LiIoCharger";
        private const string SetupPathPart = "Setups";
        private const string GraphPathPart = "Graphs";
        private const string ConfigFileName = "Config" + DefaultExtension;

        private readonly FileSystemWatcher _fileSystemWatcher;
        private readonly string _dataPath;
        private readonly string _setupPath;
        private readonly string _graphPath;

        public event EventHandler<FileSystemEventArgs> SetupFilesChanged;

        public FileService(FileSystemWatcher fileSystemWatcher)
        {
            if (fileSystemWatcher == null) throw new ArgumentNullException(nameof(fileSystemWatcher));
            _fileSystemWatcher = fileSystemWatcher;

            _dataPath = SetupPath(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), DataPathPart);
            _setupPath = SetupPath(_dataPath, SetupPathPart);
            _graphPath = SetupPath(_dataPath, GraphPathPart);

            SetupFileSystemWatcher(_setupPath);
        }

        public IEnumerable<string> GetChannelSetupNames()
        {
            try
            {
                return Directory.EnumerateFiles(_setupPath, "*.txt").Select(Path.GetFileNameWithoutExtension);
            }
            catch (Exception exception)
            {
                throw new FileServiceException(LogTools.Error(exception), exception);
            }
        }

        public string GetSaveSetupFileName()
        {
            var fileName = GetSaveFileName(Resources.ChannelInputControl.SaveSetupTitle, _setupPath);
            return fileName == null ? null : Path.GetFileNameWithoutExtension(fileName) + DefaultExtension;
        }

        public string GetLoadGraphFileName()
        {
            return GetSaveFileName(Resources.ChannelGraphControl.LoadGraphTitle, _graphPath, false);
        }

        public string GetSaveGraphFileName()
        {
            return GetSaveFileName(Resources.ChannelGraphControl.SaveGraphTitle, _graphPath);
        }

        public Configuration GetConfiguration()
        {
            return Read<Configuration>(Path.Combine(_dataPath, ConfigFileName));
        }

        public void WriteConfiguration(Configuration configuration)
        {
            Write(Path.Combine(_dataPath, ConfigFileName), configuration);
        }

        public ChannelSetup GetChannelSetup(string fileName)
        {
            return Read<ChannelSetup>(Path.ChangeExtension(Path.Combine(_setupPath, fileName), DefaultExtension));
        }

        public void WriteChannelSetup(string fileName, ChannelSetup channelSetup)
        {
            Write(Path.Combine(_setupPath, fileName), channelSetup);
        }

        public void WriteTextFile(string fileName, Func<int, string> getLineFunc)
        {
            LogTools.Enter(new { fileName });
            try
            {
                if (string.IsNullOrEmpty(fileName)) return;
                if (getLineFunc == null) return;
                var lines = 0;
                string line;
                using (var writer = new StreamWriter(fileName))
                {
                    do
                    {
                        line = getLineFunc(lines);
                        if (line != null) writer.WriteLine(line);
                        lines++;
                    } while ((line != null) && (lines < int.MaxValue));
                }
            }
            catch (Exception exception)
            {
                LogTools.Error(new { fileName }, exception);
                throw;
            }
            finally
            {
                LogTools.Exit(new { fileName });
            }
        }

        public void ReadTextFile(string fileName, Action<string> readLineAction)
        {
            LogTools.Enter(new { fileName });
            try
            {
                if (string.IsNullOrEmpty(fileName)) return;
                if (readLineAction == null) return;
                using (var reader = new StreamReader(fileName))
                {
                    while (reader.Peek() >= 0) readLineAction(reader.ReadLine());
                }
            }
            catch (Exception exception)
            {
                LogTools.Error(new { fileName }, exception);
                throw;
            }
            finally
            {
                LogTools.Exit(new { fileName });
            }
        }

        #region private

        private string GetSaveFileName(string title, string path, bool overwritePrompt = true)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = title,
                DefaultExt = DefaultExtension,
                InitialDirectory = path,
                OverwritePrompt = overwritePrompt,
                AddExtension = true
            };

            var ok = saveFileDialog.ShowDialog();
            return (ok.HasValue && ok.Value) ? saveFileDialog.FileName : null;
        }

        private string SetupPath(string part1, string part2)
        {
            var path = Path.Combine(part1, part2);
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            return path;
        }

        private T Read<T>(string fileName)
        {
            LogTools.Enter();
            try
            {
                var t = JsonConvert.DeserializeObject<T>(File.ReadAllText(fileName));
                LogTools.Exit();
                return t;
            }
            catch (Exception exception)
            {
                LogTools.Error(exception);
                return default(T);
            }
        }

        private void Write<T>(string fileName, T t)
        {
            LogTools.Enter();
            try
            {
                File.WriteAllText(fileName, JsonConvert.SerializeObject(t));
                LogTools.Exit();
            }
            catch (Exception exception)
            {
                throw new FileServiceException(LogTools.Error(exception), exception);
            }
        }

        private void SetupFileSystemWatcher(string path)
        {
            _fileSystemWatcher.Path = path;
            _fileSystemWatcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            _fileSystemWatcher.Changed += OnFileSystemChanged;
            _fileSystemWatcher.Created += OnFileSystemChanged;
            _fileSystemWatcher.Deleted += OnFileSystemChanged;
            _fileSystemWatcher.Renamed += OnFileSystemChanged;
            _fileSystemWatcher.EnableRaisingEvents = true;
        }

        private void OnFileSystemChanged(object source, FileSystemEventArgs e)
        {
            var handler = SetupFilesChanged;
            handler?.Invoke(this, e);
        }

        #endregion
    }
}
