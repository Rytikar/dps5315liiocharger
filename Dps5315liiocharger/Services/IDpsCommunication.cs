﻿using Dps5315LiIoCharger.DataObjects;
using Dps5315LiIoCharger.Dps5315;

namespace Dps5315LiIoCharger.Services
{
    public interface IDpsCommunication
    {
        ConnectInformation Connect();
        void Disconnect();
        T WriteRead<T>(IDpsRequest request) where T : class, IDpsResponse, new();
        string Port { get; }
        int BaudRate { get; }
        bool IsConnected { get; }
    }
}
