﻿using Dps5315LiIoCharger.Tools;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;

namespace Dps5315LiIoCharger.Services
{
    public class MsSerialService : BaseSerialService, ISerialService
    {
        private readonly ICrcService _crcService;
        private readonly SerialPort _serialPort;

        public MsSerialService(ICrcService crcService)
        {
            if (crcService == null) throw new ArgumentNullException(nameof(crcService));
            _crcService = crcService;

            _serialPort = new SerialPort();
        }

        public bool IsOpen => _serialPort.IsOpen;
        public string Port => IsOpen ? _serialPort.PortName : string.Empty;
        public int BaudRate => IsOpen ? _serialPort.BaudRate : 0;

        public void Open(string port, int baudRate)
        {
            LogTools.Enter(new { Port = port, BaudRate = baudRate });
            _serialPort.PortName = port;
            _serialPort.BaudRate = baudRate;
            _serialPort.Encoding = Encoding.UTF8;
            _serialPort.ReadTimeout = ReadTimeoutMs;
            _serialPort.WriteTimeout = WriteTimeoutMs;

            try
            {
                _serialPort.Open();
                LogTools.Exit(new { Port = port, BaudRate = baudRate });
            }
            catch (Exception exception)
            {
                throw new SerialServiceException(LogTools.Error(new { Port = port, BaudRate = baudRate }, exception), exception);
            }
        }

        public void Close()
        {
            LogTools.Enter();
            if (!IsOpen)
            {
                LogTools.Status("Is not open!");
                LogTools.Exit();
                return;
            }
            try
            {
                _serialPort.Close();
                LogTools.Exit();
            }
            catch (Exception exception)
            {
                throw new SerialServiceException(LogTools.Error(exception), exception);
            }
        }

        public IList<SerialInformation> GetSerialInformation()
        {
            LogTools.Enter();
            var result = new List<SerialInformation>();
            try
            {
                using (var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity"))
                {
                    var portNames = SerialPort.GetPortNames();
                    foreach (var o in searcher.Get())
                    {
                        var queryObj = (ManagementObject)o;
                        var caption = queryObj["Caption"];
                        if (caption == null) continue;
                        result.AddRange(from portName in portNames where caption.ToString().Contains($"({portName})") select new SerialInformation(portName, caption.ToString()));
                    }
                }
                LogTools.Exit();
                return result;
            }
            catch (Exception exception)
            {
                throw new SerialServiceException(LogTools.Error(exception), exception);
            }
        }

        public byte[] ReadFrame()
        {
            LogTools.Enter();
            //TODO if (!_serialPort.CanRead) throw new InvalidOperationException("Port not ready to be read from!");
            var list = new List<byte>();
            try
            {
                do { } while (_serialPort.ReadByte() != FrameStart);
                int i;
                do
                {
                    i = _serialPort.ReadByte();
                    if ((i == -1) || (i == FrameEnd)) break;

                    if (i == EscapeByte) i = _serialPort.ReadByte() & 127;
                    list.Add((byte)i);
                } while (true);

                if (i != FrameEnd) throw new InvalidOperationException("Invalid frame!");

                var data = _crcService.CheckCrc(list);

                LogTools.Exit(new { Data = BitConverter.ToString(data) });
                return data;
            }
            catch (Exception exception)
            {
                throw new SerialServiceException(LogTools.Error(exception), exception);
            }
        }

        public void WriteFrame(byte[] data)
        {
            throw new NotImplementedException();
        }
    }
}
