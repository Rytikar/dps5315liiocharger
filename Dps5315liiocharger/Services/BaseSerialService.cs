﻿namespace Dps5315LiIoCharger.Services
{
    public abstract class BaseSerialService
    {
        protected const int EscapeByte = 16;
        protected const byte FrameStart = 2;
        protected const byte FrameEnd = 3;
        protected const int ReadTimeoutMs = 1000;
        protected const int WriteTimeoutMs = 1000;
    }
}
