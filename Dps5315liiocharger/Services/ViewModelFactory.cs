﻿using Dps5315LiIoCharger.ViewModels;
using Microsoft.Practices.Unity;
using System;

namespace Dps5315LiIoCharger.Services
{
    public interface IViewModelFactory
    {
        T Resolve<T>() where T : class, IViewModel;
    }

    public class ViewModelFactory : IViewModelFactory
    {
        private readonly IUnityContainer _container;

        public ViewModelFactory(IUnityContainer container)
        {
            if (container == null) throw new ArgumentNullException(nameof(container));
            _container = container;
        }

        public T Resolve<T>() where T : class, IViewModel
        {
            return _container.Resolve<T>();
        }
    }
}
