# Introduction #

The Dps5315LiIoCharger is an application to charge batteries with the DPS5315 dual power supply from [ELV](http://www.elv.de/) with USB addon board.

* 0.9.00, beta release in the [Downloads](https://bitbucket.org/Rytikar/dps5315liiocharger/downloads) section
* Needs the [.net 4.5.2](https://www.microsoft.com/en-us/download/details.aspx?id=42642) framework installed
* The software has been developed and tested under Windows 10 on an english setup but should run from Windows 7 and up. Report localization problems, please.
* Test please and report issues in the [Issues](https://bitbucket.org/Rytikar/dps5315liiocharger/issues?status=new&status=open) section

This readme and documentation wiki under construction...

![StoppedConstantU2x3s.png](https://bitbucket.org/repo/Ek95dg/images/820743311-StoppedConstantU2x3s.png)

### A bit of history ###

In the summer of 2016 I got myself the [DPS5315 dual power supply kit](http://www.elv.de/dual-power-supply-dps-5315-komplettbausatz.html) from ELV together with it's [USB connector board](http://www.elv.de/optisch-isoliertes-usb-modul-fuer-dps5315-uo100-dps5315.html). It was a pleasure to build and worked a treat. 
The accompanying software works fine and does what is was designed for.

At the same time, I started dabbling with lithium ion batteries and fell over a [youtube video](https://www.youtube.com/watch?v=jNmlxBXEqW0) from [EEVBlog](http://www.eevblog.com/) describing how to charge those batteries with a bench power supply.
The DPS5315 has all the features needed and worked great. On the other hand, the software was missing (AFAIK) the feature of setting the output to standby when a specific charge current was reached. So, you had to watch the charge process to shut it down manually.

Me actually being more of a software guy than electronics I thought of implementing a solution myself.
Digging around the web I found out the usb protocol of the DPS5315 was not openly available, but that only stopped me for an evening. 

I was able to reverse engineer the protocol and implemented what has now evolved to Dps5315LiIoCharger (you know how those prototype names stick once you're under way, right?)

### Features ###

* Uses single channels of the DPS for two times 15V/3A charging capability or the combined series channel for one 30V/3A output.
* Handles pre charging, constant current and constant voltage charging while observing charge cutout current
* Plots charge graph while charging. Graphs can be saved and reloaded to see differences between charge runs.
* Setups can be saved and reloaded
* Translations of the user interface in English, German and Danish (as of now)

### Setup ###

### Hardware ###

![DPS_5315.jpg](https://bitbucket.org/repo/Ek95dg/images/3090963751-DPS_5315.jpg)

### Who do I talk to? ###

* dps@rosenkilde-hoppe.dk

### Thanks ###

To:

* [ELV](http://www.elv.de/)
* [SerialPortStream](https://github.com/jcurl/serialportstream)
* [OxyPlot](http://www.oxyplot.org/)
* [WPFLocalizationExtension](https://github.com/SeriousM/WPFLocalizationExtension)